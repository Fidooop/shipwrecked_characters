name = "_Shipwrecked Characters"
description = "The characters from Don't Starve:Shipwrecked ported to DST."
author = "CheeseNuggets101"
version = "3.2"
icon_atlas = "modicon.xml"
icon = "modicon.tex"
forumthread = "topic/72813-cheesenuggets-cheesy-fan-art-thread/"
api_version = 10
priority = 11

dont_starve_compatible = false
reign_of_giants_compatible = true
shipwrecked_compatible = false --Maybe one day
dst_compatible = true
client_only_mod = false
all_clients_require_mod = true
forge_compatible = true

server_filter_tags = {"shipwrecked", "characters", "walani", "warly", "wilbur", "woodlegs", "shipwrecked characters"}

mim_assets = {
    characters = {
        walani = { gender = "FEMALE", name = "Walani", title = "The Unperturbable", quote = "\"Forgive me if I don't get up. I don't want to.\"",
		aboutme = "A temperate, easygoing Surfer with love for all things chill, Walani relishes in the art of doing nothing at all." },
        warly = { gender = "MALE", name = "Warly", title = "The Culinarian", quote = "\"Nothing worthwhile is ever done on an empty stomach!\"",
		aboutme = "A grandiloquent yet solicitous Chef, Warly seeks a life of riches and rewards for his esteemed culinary." },	
		wilbur = { gender = "NEUTRAL", name = "Wilbur", title = "The Monkey King", quote = "\"Ooo ooa oah ah!\"",
		aboutme = "One might question why one would have a monkey run freely throughout the Constant. Wilbur will not answer this." },	
		woodlegs = { gender = "MALE", name = "Woodlegs", title = "The Pirate Captain", quote = "\"Don't ye mind th'scurvy. Yarr-harr-harr!\"",
		aboutme = "Woodlegs is a seasoned rambunctious sea-faring pirate on a never ending journey for the treasure he sees fit." }
    }
}

configuration_options =
{

	{
        name = "wilburrun",
        label = "Wilbur's Run",
		hover = "Disable or Enable Wilbur's run ability.",
        options = 
        {
            {description = "Enable", data = "y"},
            {description = "Disable", data = "n"},
        }, 
        default = "y",
    },		
	--[[{
        name = "woodlegs_sanity",
        label = "Woodlegs' Constant Sanity Drain",
		hover = "Disable or Enable having a constant sanity drain as Woodlegs.",
        options = 
        {
            {description = "Enable", data = "y"},
            {description = "Disable", data = "n"},
        }, 
        default = "y",
    },
	{
        name = "woodlegs_luck",
        label = "Woodlegs' Lucky Hat Treasure",
		hover = "Disable or Enable having Woodlegs' lucky hat spawn treasure.",
        options = 
        {
            {description = "Enable", data = "y"},
            {description = "Disable", data = "n"},
        }, 
        default = "y",
    },--]]
	{
        name = "wcraftpot",
        label = "Craftable Portable Crock Pot",
		hover = "Disable or Enable the ability to have a craftable portable crock pot for Warly.",
        options = 
        {
            {description = "Enable", data = "y"},
            {description = "Disable", data = "n"},
        }, 
        default = "y",
    },	
	{
        name = "surf_sanity",
        label = "Surfboard Sanity Aura",
		hover = "Disable or Enable Walani's original surfboard sanity aura.",
        options = 
        {
            {description = "Enable", data = "y"},
            {description = "Disable", data = "n"},
        }, 
        default = "n",
    },
	
}	