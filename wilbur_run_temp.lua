--Credit to Jack for working with me on this!
if GetModConfigData("wilburrun") == "y" then

local function wilburruncheck(sg)

local function ConfigureRunState(inst)
    if inst.replica.rider ~= nil and inst.replica.rider:IsRiding() then
        inst.sg.statemem.riding = true
        inst.sg.statemem.groggy = inst:HasTag("groggy")
    elseif inst.replica.inventory:IsHeavyLifting() then
        inst.sg.statemem.heavy = true
    elseif inst:HasTag("beaver") then
        inst.sg.statemem.groggy = inst:HasTag("groggy")
    elseif inst:GetSandstormLevel() >= TUNING.SANDSTORM_FULL_LEVEL and not inst.components.playervision:HasGoggleVision() then
        inst.sg.statemem.sandstorm = true
    elseif inst:HasTag("groggy") then
        inst.sg.statemem.groggy = true
    elseif inst:IsCarefulWalking() then
        inst.sg.statemem.careful = true
    end
end

	sg.states.run.onexit = function(inst)
		if inst:HasTag("wilbur") and not inst.replica.rider:IsRiding() and not inst.replica.inventory:IsHeavyLifting() then
		    inst.AnimState:SetBank("wilson")
		    inst.AnimState:SetBuild(inst.prefab)
		    inst.Transform:SetFourFaced()
		end
	end

	sg.states.run.onenter = function(inst)
        inst.sg.statemem.heavy = not inst.sg.statemem.riding and inst.replica.inventory:IsHeavyLifting()
		ConfigureRunState(inst)
        inst.components.locomotor:RunForward()
		
		   inst.sg.statemem.normal = not (
                inst.sg.statemem.riding or
                inst.sg.statemem.heavy or
                inst.sg.statemem.groggy or
                inst.sg.statemem.careful
            )

		if inst:HasTag("wilbur") and not inst:HasTag("faster") then
		
		local function GetRunStateAnim(inst)
        return (inst.sg.statemem.heavy and "heavy_walk")
        or (inst.sg.statemem.sandstorm and "sand_walk")
        or (inst.sg.statemem.groggy and "idle_walk")
        or (inst.sg.statemem.careful and "careful_walk") --Have Wilbur not run on sinkholes in future when I can?
        or "run"
        end
		
            local anim = GetRunStateAnim(inst)
            if anim == "run" then
                anim = "run_loop"
            end
            if not inst.AnimState:IsCurrentAnimation(anim) then
                inst.AnimState:PlayAnimation(anim, true)
            end
			
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
            inst.sg.statemem.riding = inst.replica.rider ~= nil and inst.replica.rider:IsRiding()
		end
		
		if not inst:HasTag("wilbur") and not inst:HasTag("faster") then
		
		local function GetRunStateAnim(inst)
        return (inst.sg.statemem.heavy and "heavy_walk")
        or (inst.sg.statemem.sandstorm and "sand_walk")
        or (inst.sg.statemem.groggy and "idle_walk")
        or (inst.sg.statemem.careful and "careful_walk")
        or "run"
        end
		
            local anim = GetRunStateAnim(inst)
            if anim == "run" then
                anim = "run_loop"
            end
            if not inst.AnimState:IsCurrentAnimation(anim) then
                inst.AnimState:PlayAnimation(anim, true)
            end
			
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
            inst.sg.statemem.riding = inst.replica.rider ~= nil and inst.replica.rider:IsRiding()
		end

		 if inst:HasTag("faster") and inst:HasTag("wilbur") then 
			inst.Transform:SetSixFaced() 
			inst.AnimState:SetBank("wilbur_run") 
			inst.AnimState:SetBuild("wilbur_run")
                	inst.AnimState:PlayAnimation("run_loop", true)
            		inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())

	    		if inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) ~= nil then
                		inst.AnimState:Show("TAIL_carry")
                		inst.AnimState:Hide("TAIL_normal")
	    		end
	    		if inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) == nil then
                		inst.AnimState:Hide("TAIL_carry")
                		inst.AnimState:Show("TAIL_normal")
	    		end
		end
	end

	sg.states.run.onupdate = function(inst)
            inst.components.locomotor:RunForward()

		 if inst:HasTag("faster") and inst:HasTag("wilbur") and not inst.replica.rider:IsRiding() and not inst.replica.inventory:IsHeavyLifting() then
			inst.Transform:SetSixFaced() 
			inst.AnimState:SetBank("wilbur_run") 
			inst.AnimState:SetBuild("wilbur_run")
                	inst.AnimState:PushAnimation("run_loop", true)
            		inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
	    		if inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) ~= nil then
                		inst.AnimState:Show("TAIL_carry")
                		inst.AnimState:Hide("TAIL_normal")
	    		end
	    		if inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) == nil then
                		inst.AnimState:Hide("TAIL_carry")
                		inst.AnimState:Show("TAIL_normal")
	    		end
		end
	end
end

AddStategraphPostInit("wilson", wilburruncheck)
AddStategraphPostInit("wilson_client", wilburruncheck)
end