local STRINGS = GLOBAL.STRINGS
local require = GLOBAL.require

RemapSoundEvent( "dontstarve/characters/walani/talk_LP", "walani/walani/talk_LP" )
RemapSoundEvent( "dontstarve/characters/walani/ghost_LP", "walani/walani/ghost_LP" )
RemapSoundEvent( "dontstarve/characters/walani/hurt", "walani/walani/hurt" )
RemapSoundEvent( "dontstarve/characters/walani/death_voice", "walani/walani/death_voice" )
RemapSoundEvent( "dontstarve/characters/walani/emote", "walani/walani/emote" )
RemapSoundEvent( "dontstarve/characters/walani/yawn", "walani/walani/yawn" )
RemapSoundEvent( "dontstarve/characters/walani/eye_rub_vo", "walani/walani/eye_rub_vo" )
RemapSoundEvent( "dontstarve/characters/walani/pose", "walani/walani/pose" )
RemapSoundEvent( "dontstarve/characters/walani/carol", "walani/walani/carol" )

RemapSoundEvent( "dontstarve/characters/warly/talk_LP", "warly/warly/talk_LP" )
RemapSoundEvent( "dontstarve/characters/warly/ghost_LP", "warly/warly/ghost_LP" )
RemapSoundEvent( "dontstarve/characters/warly/hurt", "warly/warly/hurt" )
RemapSoundEvent( "dontstarve/characters/warly/death_voice", "warly/warly/death_voice" )
RemapSoundEvent( "dontstarve/characters/warly/emote", "warly/warly/emote" )
RemapSoundEvent( "dontstarve/characters/warly/yawn", "warly/warly/yawn" )
RemapSoundEvent( "dontstarve/characters/warly/eye_rub_vo", "warly/warly/eye_rub_vo" )
RemapSoundEvent( "dontstarve/characters/warly/pose", "warly/warly/pose" )
RemapSoundEvent( "dontstarve/characters/warly/carol", "warly/warly/carol" )

RemapSoundEvent( "dontstarve/characters/wilbur/talk_LP", "wilbur/wilbur/talk_LP" )
RemapSoundEvent( "dontstarve/characters/wilbur/ghost_LP", "wilbur/wilbur/ghost_LP" )
RemapSoundEvent( "dontstarve/characters/wilbur/hurt", "wilbur/wilbur/hurt" )
RemapSoundEvent( "dontstarve/characters/wilbur/death_voice", "wilbur/wilbur/death_voice" )
RemapSoundEvent( "dontstarve/characters/wilbur/emote", "wilbur/wilbur/emote" )
RemapSoundEvent( "dontstarve/characters/wilbur/yawn", "wilbur/wilbur/yawn" )
RemapSoundEvent( "dontstarve/characters/wilbur/eye_rub_vo", "wilbur/wilbur/eye_rub_vo" )
RemapSoundEvent( "dontstarve/characters/wilbur/pose", "wilbur/wilbur/pose" )
RemapSoundEvent( "dontstarve/characters/wilbur/carol", "wilbur/wilbur/carol" )

RemapSoundEvent( "dontstarve/characters/woodlegs/talk_LP", "woodlegs/woodlegs/talk_LP" )
RemapSoundEvent( "dontstarve/characters/woodlegs/ghost_LP", "woodlegs/woodlegs/ghost_LP" )
RemapSoundEvent( "dontstarve/characters/woodlegs/hurt", "woodlegs/woodlegs/hurt" )
RemapSoundEvent( "dontstarve/characters/woodlegs/death_voice", "woodlegs/woodlegs/death_voice" )
RemapSoundEvent( "dontstarve/characters/woodlegs/emote", "woodlegs/woodlegs/emote" )
RemapSoundEvent( "dontstarve/characters/woodlegs/yawn", "woodlegs/woodlegs/yawn" )
RemapSoundEvent( "dontstarve/characters/woodlegs/eye_rub_vo", "woodlegs/woodlegs/eye_rub_vo" )
RemapSoundEvent( "dontstarve/characters/woodlegs/pose", "woodlegs/woodlegs/pose" )
RemapSoundEvent( "dontstarve/characters/woodlegs/carol", "woodlegs/woodlegs/carol" )

--Walani
GLOBAL.STRINGS.CHARACTER_TITLES.walani = "The Unperturbable"
GLOBAL.STRINGS.CHARACTER_NAMES.walani = "Walani"
GLOBAL.STRINGS.CHARACTER_DESCRIPTIONS.walani = "*Loves surfing\n*Dries off quickly\n*Is a pretty chill gal"
GLOBAL.STRINGS.CHARACTER_QUOTES.walani =  "\"Forgive me if I don't get up. I don't want to.\""
GLOBAL.STRINGS.CHARACTERS.WALANI = require "speech_walani"
GLOBAL.STRINGS.NAMES.WALANI = "Walani"
GLOBAL.STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.walani = "*X slows enemies by 25%\n*X stuns enemies while boosted\n\n\nExpertise:\nDarts" 
--Still wish Klei did all this instead, sigh.

--Character interaction strings
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WALANI = 
{
            GENERIC = "Greetings, %s!",
            ATTACKER = "%s looks shifty...",
            MURDERER = "Murderer!",
            REVIVER = "%s, friend of ghosts.",
            GHOST = "I better concoct a heart for %s.",
			FIRESTARTER = "Was that an accident, %s?",
}
STRINGS.CHARACTERS.WILLOW.DESCRIBE.WALANI = 
{
            GENERIC = "Hi %s!",
            ATTACKER = "I thought you enjoyed chilling, %s!",
            MURDERER = "%s! Take it easy and maybe chillax!",
            REVIVER = "%s is pretty chill with a whole bunch of things! Like ghosts!",
            GHOST = "I better get a heart for %s.",
			FIRESTARTER = "Didn't know you had it in you, %s!",
}
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.WALANI = 
{
            GENERIC = "Is tiny chill lady, %s! Hello!",
            ATTACKER = "Does tiny chill lady want to fight?",
            MURDERER = "%s is no match for Wolfgang!",
            REVIVER = "%s is very nice, tells Wolfgang sea stories!",
            GHOST = "Wolfgang will get raw pump-y heart for you!",
			FIRESTARTER = "Wolfgang thought he could trust %s!",
}
STRINGS.CHARACTERS.WENDY.DESCRIBE.WALANI = 
{
            GENERIC = "How do you do, %s?",
            ATTACKER = "%s is a madwoman.",
            MURDERER = "I'll send you some place much nicer than this, %s.",
            REVIVER = "Abigail likes you, %s.",
            GHOST = "If I get you a heart you won't leave us, right %s?",
			FIRESTARTER = "Douse your fires, %s.",
}
STRINGS.CHARACTERS.WX78.DESCRIBE.WALANI = 
{
            GENERIC = "DETECTING... %s!",
            ATTACKER = "%s IS PUSHING MY BUTTONS",
            MURDERER = "I CANNOT BE 'CHILLAXED' TO DEATH",
            REVIVER = "%s HAS BEEN HELPFUL. I WILL HOLD OFF ON HER DESTRUCTION",
            GHOST = "HAHA, %s. YOU DIED.",
			FIRESTARTER = "%s HAS TURNNED FROM WATER AND SITTING AROUND TO BURNING THINGS. NICE.",
}
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.WALANI = 
{
            GENERIC = "Ah, greetings dear %s!",
            ATTACKER = "You like to test boundaries, don't you dear?",
            MURDERER = "You WILL respect your elders, %s!",
            REVIVER = "You're doing so well, %s.",
            GHOST = "With all that chilling you do I recommend a jacket next time, %s!",
			FIRESTARTER = "You're the last person I'd expect to burn something. Tsk.",
}
STRINGS.CHARACTERS.WOODIE.DESCRIBE.WALANI = 
{
            GENERIC = "%s! Hey bud!",
            ATTACKER = "I'm guessing that's a cancellation on my swimming lesson.",
            MURDERER = "Enemy of the forest! Traitor!",
            REVIVER = "%s, you're a pretty chill gal.",
            GHOST = "First we'll get you a heart, then we'll get you some bacon, eh %s?",
			FIRESTARTER = "You're gonna burn your board, %s.",
}
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.WALANI = 
{
            GENERIC = "Greetings, Ms. %s.",
            ATTACKER = "%s is not my ally...",
            MURDERER = "Do not start fights you cannot win!",
            REVIVER = "If %s does anything, it's being shockingly decent to keep around. For now.",
            GHOST = "We will require a heart to bring you back, %s.",
			FIRESTARTER = "%s, wouldn't you much prefer water over fire?",
}
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.WALANI = 
{
	        GENERIC = "Good health to you, %s!",
	        ATTACKER = "Should it come to blows, I wish only that the best maiden triumph.",
	        MURDERER = "%s, your body is weak but your heart is strong! Fight!",
	        REVIVER = "Freya smiles on %s.",
	        GHOST = "A heart shall wrench %s back from the jaws of death!",
	        FIRESTARTER = "%s has turned from water to flame!",
}
STRINGS.CHARACTERS.WEBBER.DESCRIBE.WALANI = 
{
	        GENERIC = "Hi %s! Nice weather we're having!",
	        ATTACKER = "Why are you being so mean, %s?",
	        MURDERER = "Do what you always do and chill out, %s!",
	        REVIVER = "%s said she would teach us how to surf!",
	        GHOST = "Uh-oh! Does that hurt, %s?",
	        FIRESTARTER = "%s, why are you lighting fires?",
}
STRINGS.CHARACTERS.WINONA.DESCRIBE.WALANI = 
{
            GENERIC = "How's life treating ya, %s?",
            ATTACKER = "Didn't know ya had it in ya, %s!",
            MURDERER = "Murder isn't the kinda work I've been tellin' ya to do %s! Get'er!",
            REVIVER = "You're good people, %s. Ya could do some more workin' though!",
            GHOST = "You'd probably get cut worse out in the waters, %s.",
			FIRESTARTER = "A fire? That's not like ya, %s. Something up?",
}

GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SURFBOARD = "Radical."
GLOBAL.STRINGS.CHARACTERS.WILLOW.DESCRIBE.SURFBOARD = "Just a dumb colored board."
GLOBAL.STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.SURFBOARD = "How will tiny plank support me?"
GLOBAL.STRINGS.CHARACTERS.WENDY.DESCRIBE.SURFBOARD = "It's not supposed to be here... is it?"
GLOBAL.STRINGS.CHARACTERS.WX78.DESCRIBE.SURFBOARD = "BARELY ADEQUATE VESSEL FOR SEA TRAVEL"
GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.SURFBOARD = "A buoyant, maneuverable wooden surfing board. It doesn't seem concrete in this world."
GLOBAL.STRINGS.CHARACTERS.WOODIE.DESCRIBE.SURFBOARD = "Maybe I oughta learn to surf, aye 'Luce?"
GLOBAL.STRINGS.CHARACTERS.WAXWELL.DESCRIBE.SURFBOARD = "Looks much too precarious for my liking."
GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.SURFBOARD = "Tropical island aesthetic for the base!"
GLOBAL.STRINGS.CHARACTERS.WEBBER.DESCRIBE.SURFBOARD = "We can't surf out here!"
GLOBAL.STRINGS.CHARACTERS.WINONA.DESCRIBE.SURFBOARD = "Now what purpose could this possibly serve out here?"

--Warly
GLOBAL.STRINGS.CHARACTER_TITLES.warly = "The Culinarian"
GLOBAL.STRINGS.CHARACTER_NAMES.warly = "Warly"
GLOBAL.STRINGS.CHARACTER_DESCRIPTIONS.warly = "*Has a refined palate\n*Cooks in custom kitchenware\n*Brings a stylish chef pouch"
GLOBAL.STRINGS.CHARACTER_QUOTES.warly =  "\"Nothing worthwhile is ever done on an empty stomach!\""
GLOBAL.STRINGS.CHARACTERS.WARLY = require "speech_warly"
GLOBAL.STRINGS.NAMES.WARLY = "Warly"
GLOBAL.STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.warly = "*Portable Crock Pot provides a buff while placed, after dealing sufficient damage\n\nExpertise:\nDarts, Staves" 

STRINGS.CHARACTERS.GENERIC.DESCRIBE.WARLY = 
{
            GENERIC = "Good day, %s!",
            ATTACKER = "%s looks like they're planning to cook me!",
            MURDERER = "%s has been overcooked!",
            REVIVER = "%s is cooking up friendships.",
            GHOST = "Looks like you've been cooked, %s.",
			FIRESTARTER = "I think you're cooking the wrong stuff, %s.",
}
STRINGS.CHARACTERS.WILLOW.DESCRIBE.WARLY = 
{
            GENERIC = "Hi %s!",
            ATTACKER = "You're only fueling the fire, %s!",
            MURDERER = "Burn the murderer! And take his food!",
            REVIVER = "Who do ghosts call? %s! For a some good food!",
            GHOST = "Hey %s, I'll get you a heart if you let me use your crock pot!",
			FIRESTARTER = "Now that's how you start a cooking fire, %s!",
}
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.WARLY = 
{
            GENERIC = "Is tiny chef man, %s! Hello!",
            ATTACKER = "Ha ha! %s should not pick fight with dumb food!",
            MURDERER = "%s is killer!",
            REVIVER = "%s is nice, cooks good grub for Wolfgang.",
            GHOST = "Do not hurt, chef man. Wolfgang get heart!",
			FIRESTARTER = "Please keep burny fires in pot!",
}
STRINGS.CHARACTERS.WENDY.DESCRIBE.WARLY = 
{
            GENERIC = "How are you coping, %s?",
            ATTACKER = "I don't trust %s. Fear makes people dangerous.",
            MURDERER = "%s, this is the end... for you!",
            REVIVER = "Abigail likes you, %s.",
            GHOST = "We'll get a heart, but are you sure you want to come back, %s?",
			FIRESTARTER = "You must have used too much fire for your cooking, %s.",
}
STRINGS.CHARACTERS.WX78.DESCRIBE.WARLY = 
{
            GENERIC = "DETECTING... %s!",
            ATTACKER = "DO YOU EAT OTHER ORGANICS WHEN YOU'RE DONE TENDERIZING THEM TOO? HAHA.",
            MURDERER = "PATHETIC COOKING IS NOTHING IN THE FACE OF KILLER ROBOTS!",
            REVIVER = "THE COOKING FLESHLING %s MAY BE WORTH KEEPING AROUND",
            GHOST = "WHERE IS YOUR METAL COOKING SERVANT NOW, %s? HA. HA.",
			FIRESTARTER = "%s IS NOW COOKING IN MY PREFERRED STYLE",
}
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.WARLY = 
{
            GENERIC = "Ah, greetings dear %s!",
            ATTACKER = "You're not cooking up trouble, are you %s?",
            MURDERER = "I will defend myself from you, foul cad!",
            REVIVER = "I appreciate your commitment to group survival, %s.",
            GHOST = "%s, don't watch that crock pot of yours too closely now. Tsk.",
			FIRESTARTER = "Lighting fires are we now, %s? Keep your crock pot enclosed.",
}
STRINGS.CHARACTERS.WOODIE.DESCRIBE.WARLY = 
{
            GENERIC = "%s! How ya doin', buddy?",
            ATTACKER = "%s could learn some manners...",
            MURDERER = "This is gonna be a clear cut!",
            REVIVER = "%s, you're an alright guy.",
            GHOST = "With a heart you'll be tip-top in no time, %s.",
			FIRESTARTER = "I'm no expert but that is not how you start cooking.",
}
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.WARLY = 
{
            GENERIC = "Good day, Mr. %s.",
            ATTACKER = "%s is stirring up trouble...",
            MURDERER = "Murderous fiend! Taste my wrath!",
            REVIVER = "%s has excellent command of the dark culinary arts.",
            GHOST = "You know the price of revival as well as I do, %s.",
			FIRESTARTER = "That is not where cooking fires go, %s.",
}
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.WARLY = 
{
	        GENERIC = "Good health and cooking to you, %s!",
	        ATTACKER = "Your honor wavers, %s.",
	        MURDERER = "May we meet again in Valhalla!",
	        REVIVER = "%s has Andhrímnir's blessing.",
	        GHOST = "Meditate on Andhrímnir's blessings, %s. I'll find a heart..",
	        FIRESTARTER = "Have you misplaced your cooking pot, %s?",
}
STRINGS.CHARACTERS.WEBBER.DESCRIBE.WARLY = 
{
	        GENERIC = "Hello Mr. %s!",
	        ATTACKER = "Uhh, Mr. %s might have other things on the menu tonight...",
	        MURDERER = "Take it easy, %s.",
	        REVIVER = "Mr. %s makes good meals for us! They're tasty!",
	        GHOST = "Don't worry Mr. %s, we'll find you a heart!",
	        FIRESTARTER = "%s, what are you cooking this time?",
}
STRINGS.CHARACTERS.WINONA.DESCRIBE.WARLY = 
{
            GENERIC = "Hey there, %s. Cookin' anything good up?",
            ATTACKER = "%s's food must be going all to their head.",
            MURDERER = "Butcher! Get'em!",
            REVIVER = "You got a good head on your shoulders, chef.",
            GHOST = "Shake it off, %s, we got grub to eat!",
			FIRESTARTER = "Your cooking skills don't advance to forest fires, do they %s?",
}

GLOBAL.STRINGS.FRESHFRUITCREPES = "Fresh Fruit Crepes"
GLOBAL.STRINGS.NAMES.FRESHFRUITCREPES = "Fresh Fruit Crepes"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.FRESHFRUITCREPES = "Sugary fruit! Part of a balanced breakfast."
GLOBAL.STRINGS.CHARACTERS.WILLOW.DESCRIBE.FRESHFRUITCREPES = "Ooo la la."
GLOBAL.STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.FRESHFRUITCREPES = "Manly midmorning brunch."
GLOBAL.STRINGS.CHARACTERS.WENDY.DESCRIBE.FRESHFRUITCREPES = "Could I get this à la mode?"
GLOBAL.STRINGS.CHARACTERS.WX78.DESCRIBE.FRESHFRUITCREPES = "READY FOR DELICIOUS CONSUMPTION"
GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.FRESHFRUITCREPES = "Sticky fingers will ensue."
GLOBAL.STRINGS.CHARACTERS.WOODIE.DESCRIBE.FRESHFRUITCREPES = "I can get maple syrup on that?"
GLOBAL.STRINGS.CHARACTERS.WAXWELL.DESCRIBE.FRESHFRUITCREPES = "Deserves to be eaten with silverware. Sadly, I have just my hands."
GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.FRESHFRUITCREPES = "So light and airy!"
GLOBAL.STRINGS.CHARACTERS.WEBBER.DESCRIBE.FRESHFRUITCREPES = "Cripes! We can't wait for these crepes!"
GLOBAL.STRINGS.CHARACTERS.WINONA.DESCRIBE.FRESHFRUITCREPES = "These are some fancy good eats, chef!"

GLOBAL.STRINGS.MONSTERTARTARE = "Monster Tartare"
GLOBAL.STRINGS.NAMES.MONSTERTARTARE = "Monster Tartare"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.MONSTERTARTARE = "There's got to be something else to eat around here."
GLOBAL.STRINGS.CHARACTERS.WILLOW.DESCRIBE.MONSTERTARTARE = "Gross!"
GLOBAL.STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.MONSTERTARTARE = "Chef make good strong smelly meal!"
GLOBAL.STRINGS.CHARACTERS.WENDY.DESCRIBE.MONSTERTARTARE = "Looks good, tastes horrible."
GLOBAL.STRINGS.CHARACTERS.WX78.DESCRIBE.MONSTERTARTARE = "I REFUSE TO EAT FLESHLING COOKED FOOD MATERIAL"
GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.MONSTERTARTARE = "Monster meat, dressed up fancy."
GLOBAL.STRINGS.CHARACTERS.WOODIE.DESCRIBE.MONSTERTARTARE = "Isn't there anything else to eat?"
GLOBAL.STRINGS.CHARACTERS.WAXWELL.DESCRIBE.MONSTERTARTARE = "It's less than appetizing."
GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.MONSTERTARTARE = "Still quivering."
GLOBAL.STRINGS.CHARACTERS.WEBBER.DESCRIBE.MONSTERTARTARE = "Looks amazing!"
GLOBAL.STRINGS.CHARACTERS.WINONA.DESCRIBE.MONSTERTARTARE = "I have some complaints for the chef..."

GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.PORTABLECOOKPOT_ITEM = "Now we're cookin'!"
GLOBAL.STRINGS.CHARACTERS.WILLOW.DESCRIBE.PORTABLECOOKPOT_ITEM = "Just a dumb pot."
GLOBAL.STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.PORTABLECOOKPOT_ITEM = "Chef will cook good meal for friends, meal like home!"
GLOBAL.STRINGS.CHARACTERS.WENDY.DESCRIBE.PORTABLECOOKPOT_ITEM = "For when the hunger gnaws at my tiny stomach..."
GLOBAL.STRINGS.CHARACTERS.WX78.DESCRIBE.PORTABLECOOKPOT_ITEM = "WE ARE KIN, YOU AND I"
GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.PORTABLECOOKPOT_ITEM = "It will be nice to eat some properly prepared food."
GLOBAL.STRINGS.CHARACTERS.WOODIE.DESCRIBE.PORTABLECOOKPOT_ITEM = "Sure makes the camp smell nice."
GLOBAL.STRINGS.CHARACTERS.WAXWELL.DESCRIBE.PORTABLECOOKPOT_ITEM = "An instrument of the dark culinary arts."
GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.PORTABLECOOKPOT_ITEM = "Cooking, a noble profession."
GLOBAL.STRINGS.CHARACTERS.WEBBER.DESCRIBE.PORTABLECOOKPOT_ITEM = "Makes yummies!"
GLOBAL.STRINGS.CHARACTERS.WINONA.DESCRIBE.PORTABLECOOKPOT_ITEM = "I'm ready for some good cookin'!"

GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.CHEFPACK = "His bag of chef's tricks!"
GLOBAL.STRINGS.CHARACTERS.WILLOW.DESCRIBE.CHEFPACK = "A chef hat for a backpack!"
GLOBAL.STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.CHEFPACK = "Is funny chef hat."
GLOBAL.STRINGS.CHARACTERS.WENDY.DESCRIBE.CHEFPACK = "Only a true chef knows its tricks."
GLOBAL.STRINGS.CHARACTERS.WX78.DESCRIBE.CHEFPACK = "CULINARY ANOMALY DETECTED"
GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.CHEFPACK = "Perfectly insulated."
GLOBAL.STRINGS.CHARACTERS.WOODIE.DESCRIBE.CHEFPACK = "Good for long hikes."
GLOBAL.STRINGS.CHARACTERS.WAXWELL.DESCRIBE.CHEFPACK = "It puts its contents into hibernation."
GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.CHEFPACK = "The companion of a wise chef."
GLOBAL.STRINGS.CHARACTERS.WEBBER.DESCRIBE.CHEFPACK = "It doesn't fit on our head, only on our back."
GLOBAL.STRINGS.CHARACTERS.WINONA.DESCRIBE.CHEFPACK = "It'll keep all those ingredients cool."

--Warly placed crock pot strings
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.PORTABLECOOKPOT = 
		{
            COOKING_LONG = "That's going to take a while.",
            COOKING_SHORT = "It's almost done!",
            DONE = "Mmmmm! It's ready to eat!",
            EMPTY = "He never leaves home without it.",
        }
GLOBAL.STRINGS.CHARACTERS.WILLOW.DESCRIBE.PORTABLECOOKPOT = 
		{
			COOKING_LONG = "The fire still has quite a bit of work to do.",
			COOKING_SHORT = "The fire is doing its thing!",
			DONE = "Fire makes everything better. Mmm!",
			EMPTY = "All food must be cleansed with fire.",
		}
GLOBAL.STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.PORTABLECOOKPOT = 
		{
			COOKING_LONG = "This take long time.",
			COOKING_SHORT = "Is almost cook!",
			DONE = "Is time to eat!",
			EMPTY = "Chef man cooks good meals for friends, meals like home!",
        }
GLOBAL.STRINGS.CHARACTERS.WENDY.DESCRIBE.PORTABLECOOKPOT = 
		{
			COOKING_LONG = "Whatever's in there isn't dead yet.",
			COOKING_SHORT = "It needs to boil.",
			DONE = "Finally, food.",
			EMPTY = "As empty as my soul.",
		}
GLOBAL.STRINGS.CHARACTERS.WX78.DESCRIBE.PORTABLECOOKPOT = 
		{
			COOKING_LONG = "MORE TIME IS REQUIRED",
			COOKING_SHORT = "IT IS ALMOST COMPLETE, I WILL FREE YOU FROM THE CHEF'S MEATY HANDS",
			DONE = "THE COOKING PROCESS IS DONE",
			EMPTY = "TO REFINE MEATS AND VEGETABLES INTO MORE ROBUST FORMS",
		}
GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.PORTABLECOOKPOT = 
        {
            COOKING_LONG = "It's got a bit to go before it's ready.",
            COOKING_SHORT = "Almost done!",
            DONE = "Supper time!",
            EMPTY = "It's one of the more sanity kitchenware I've seen. Apt to cook a fine meal.",
        }
GLOBAL.STRINGS.CHARACTERS.WOODIE.DESCRIBE.PORTABLECOOKPOT = 
		{
			COOKING_LONG = "It'll be a bit longer, eh?",
			COOKING_SHORT = "Oh boy! Here it comes!",
			DONE = "Time for supper!",
			EMPTY = "Someone get the chef to cook us up some grub.",
		}
GLOBAL.STRINGS.CHARACTERS.WAXWELL.DESCRIBE.PORTABLECOOKPOT = 
		{
			COOKING_LONG = "A true culinarian has patience when cooking like none other.",
			COOKING_SHORT = "Here it comes!",
			DONE = "Finally, some quality grub.",
			EMPTY = "Just the thought makes my mouth water.",
		}
GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.PORTABLECOOKPOT = 
		{
			COOKING_LONG = "The chef's culinary patience is beyond my own.",
			COOKING_SHORT = "I can smell the victorious stench of fresh food coming!",
			DONE = "What have we here?",
			EMPTY = "Nothing in there. Where's the chef!",
		}
GLOBAL.STRINGS.CHARACTERS.WEBBER.DESCRIBE.PORTABLECOOKPOT = 
		{
			COOKING_LONG = "It won't be done for a while.",
			COOKING_SHORT = "Almost ready!",
			DONE = "Warly's cooked us supper!",
			EMPTY = "Warly doesn't like us touching it without his supervision.",
		}
GLOBAL.STRINGS.CHARACTERS.WINONA.DESCRIBE.PORTABLECOOKPOT = 
		{
			COOKING_LONG = "Still got a bit of a wait.",
			COOKING_SHORT = "Get Warly! His cooking's almost ready!",
			DONE = "Soup's on!",
			EMPTY = "Good to have some extra hands cookin' around here.",
        }

--Wilbur
GLOBAL.STRINGS.CHARACTER_TITLES.wilbur = "The Monkey King"
GLOBAL.STRINGS.CHARACTER_NAMES.wilbur = "Wilbur"
GLOBAL.STRINGS.CHARACTER_DESCRIPTIONS.wilbur = "*Can't talk\n*Slow as biped, but fast as quadruped\n*Is a monkey"
GLOBAL.STRINGS.CHARACTER_QUOTES.wilbur =  "\"Ooo ooa oah ah!\""
GLOBAL.STRINGS.CHARACTERS.WILBUR = require "speech_wilbur"
GLOBAL.STRINGS.NAMES.WILBUR = "Wilbur"
GLOBAL.STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.wilbur = "*Accompanied by a feisty splumonkey\n*Can periodically throw manure\n\n\nExpertise:\nMelee, Darts"

STRINGS.CHARACTERS.GENERIC.DESCRIBE.WILBUR = 
{
            GENERIC = "Good day to you, %s!",
            ATTACKER = "%s has gone bananas!",
            MURDERER = "%s's gone berserk!",
            REVIVER = "%s, friend of ghosts. Monkey ghosts.",
            GHOST = "%s could use a heart and some bananas.",
			FIRESTARTER = "Someone needs to teach you fire safety, and quick.",
}
STRINGS.CHARACTERS.WILLOW.DESCRIBE.WILBUR = 
{
            GENERIC = "Hi %s!",
            ATTACKER = "%s must be really hungry! Who want to feed em?",
            MURDERER = "Monster Monkey! Burn them!",
            REVIVER = "I love having %s jumping around!",
            GHOST = "I better get a heart for %s. Monkeys have hearts, right?",
			FIRESTARTER = "Your fires are wild, %s!",
}
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.WILBUR = 
{
            GENERIC = "Is tiny monkey man, %s! Hello!",
            ATTACKER = "Ah! Monkey man is try to fight me!",
            MURDERER = "Monkey man %s is killer! Wolfgang is run!",
            REVIVER = "%s is nice monkey man.",
            GHOST = "Wolfgang will get raw pump-y heart for you!",
			FIRESTARTER = "Please no. Please no burnings monkey man!",
}
STRINGS.CHARACTERS.WENDY.DESCRIBE.WILBUR = 
{
            GENERIC = "How do you do, %s?",
            ATTACKER = "%s is unstable.",
            MURDERER = "%s must be put down.",
            REVIVER = "Abigail says she understands you, %s.",
            GHOST = "A heart could return %s to this world if they so wished...",
			FIRESTARTER = "%s tires of going unnoticed.",
}
STRINGS.CHARACTERS.WX78.DESCRIBE.WILBUR = 
{
            GENERIC = "DETECTING... %s!",
            ATTACKER = "TYPICAL MONKEY PROTOCOL",
            MURDERER = "PREPARE TO BE EXTERMINATED %s",
            REVIVER = "THE ROYAL MONKEY %s HAS SERVED ME WELL",
            GHOST = "THIS FLESHLING IS DUMBER THAN MOST",
			FIRESTARTER = "%s HAS BEEN BURNING THINGS. NICE.",
}
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.WILBUR = 
{
            GENERIC = "Ah, the monkey lad. Greetings, dear %s!",
            ATTACKER = "%s, no monkeying around!",
            MURDERER = "They'll tell tales of your defeat, %s!",
            REVIVER = "You've been well trained, %s.",
            GHOST = "Poor dear. %s needs a heart to anchor them to this plane just like the rest of us.",
			FIRESTARTER = "Oh dear. Who gave you matches, %s?",
}
STRINGS.CHARACTERS.WOODIE.DESCRIBE.WILBUR = 
{
            GENERIC = "%s! Hi little buddy!",
            ATTACKER = "Hey %s, can't we all just get a-log?",
            MURDERER = "'Luce gave me the go-ahead! You're in trouble now, %s!",
            REVIVER = "You're an alright monkey, bud. You oughta take a bath though.",
            GHOST = "Someone should rub a heart on %s.",
			FIRESTARTER = "I better not see any more fires, little buddy.",
}
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.WILBUR = 
{
            GENERIC = "Greetings, Mx. %s.",
            ATTACKER = "%s is looking downright feral...",
            MURDERER = "%s, you bare no rights to that crown!",
            REVIVER = "%s is an effective ally... For a monkey.",
            GHOST = "%s needs heart-shaped assistance.",
			FIRESTARTER = "Never let a monkey play around with a torch.",
}
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.WILBUR = 
{
	       GENERIC = "Blessings upon you, monkey %s!",
	       ATTACKER = "I kneel for no king, not even monkey ones!",
	       MURDERER = "I've felled greater creatures than you, %s! Let us fight!",
	       REVIVER = "%s has a noble heart.",
	       GHOST = "%s's restless spirit could be revived with a heart.",
	       FIRESTARTER = "%s seems to have mistakenly fumbled a torch.",
}
STRINGS.CHARACTERS.WEBBER.DESCRIBE.WILBUR = 
{
	       GENERIC = "Hi %s! Lookin' extra kingly today!",
	       ATTACKER = "Are you angry at me, %s?",
	       MURDERER = "You're no longer our friend, %s!",
	       REVIVER = "%s is a super nice friend, even if they don't always understand us!",
	       GHOST = "Don't worry, %s! Our spider friends will help bring you back!",
	       FIRESTARTER = "%s, no!! Fire can hurt you!",
}
STRINGS.CHARACTERS.WINONA.DESCRIBE.WILBUR = 
{
            GENERIC = "Is that a monkey? They even have monkeys in this place!",
            ATTACKER = "That monkey's lookin' mighty feral!",
            MURDERER = "Yikes! Feral monkey!",
            REVIVER = "Thanks for the assist, monkey.",
            GHOST = "Nothing big, at least for a monkey. You'll be fine.",
			FIRESTARTER = "That monkey's got a torch!",
}

--Woodlegs:
GLOBAL.STRINGS.CHARACTER_TITLES.woodlegs = "The Pirate Captain"
GLOBAL.STRINGS.CHARACTER_NAMES.woodlegs = "Woodlegs"
GLOBAL.STRINGS.CHARACTER_DESCRIPTIONS.woodlegs = "*Has his lucky hat\n*Has his lucky cutlass\n*Pirate"
GLOBAL.STRINGS.CHARACTER_QUOTES.woodlegs =  "\"Don't ye mind th'scurvy. Yarr-harr-harr!\""
GLOBAL.STRINGS.CHARACTERS.WOODLEGS = require "speech_woodlegs"
GLOBAL.STRINGS.NAMES.WOODLEGS = "Woodlegs"
GLOBAL.STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.woodlegs = "*All armor attributes 10% more effective\n\n\nExpertise:\nMelee, Darts"

STRINGS.CHARACTERS.GENERIC.DESCRIBE.WOODLEGS = 
{
            GENERIC = "Greetings, %s!",
            ATTACKER = "%s is taking their pirate thing too seriously.",
            MURDERER = "%s has gone full cutthroat scallywag!",
            REVIVER = "%s, sailing friendships!",
            GHOST = "%s could use a heart, but they'd prefer gold.",
			FIRESTARTER = "Just doing classic pirate stuff, aye %s?",
}
STRINGS.CHARACTERS.WILLOW.DESCRIBE.WOODLEGS = 
{
            GENERIC = "Hi %s!",
            ATTACKER = "You're a bad scallywag, %s!",
            MURDERER = "Murderer! Burn them!",
            REVIVER = "%s is a big softie!",
            GHOST = "I better get %s a harrrt!",
			FIRESTARTER = "Burn it all, %s! Burn it!",
}
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.WOODLEGS = 
{
            GENERIC = "Is white curvy beard! Hello!",
            ATTACKER = "Does curvy beardman %s want to fight?",
            MURDERER = "Drop sword and fight curvy beardman!",
            REVIVER = "%s is nice man with magnificent beard.",
            GHOST = "Wolfgang will get heart for you, curvy beard!",
			FIRESTARTER = "Be careful! Do not burn, curvy beard!",
}
STRINGS.CHARACTERS.WENDY.DESCRIBE.WOODLEGS = 
{
            GENERIC = "How do you do, %s?",
            ATTACKER = "%s is plotting something dastardly with that sword.",
            MURDERER = "%s is a cutthroat killer. As Abigail predicted.",
            REVIVER = "Abigail likes you, %s.",
            GHOST = "A heart could return %s to this world...",
			FIRESTARTER = "Fire setting does not seem in your best interest...",
}
STRINGS.CHARACTERS.WX78.DESCRIBE.WOODLEGS = 
{
            GENERIC = "DETECTING... %s!",
            ATTACKER = "PIRATE FLESHLINGS GET RESTLESS ON LAND",
            MURDERER = "THIS WILL BE THE LAST INSTANCE OF PLANK WALKING, %s. DIE!",
            REVIVER = "%s HAS PROVEN HIMSELF USEFUL",
            GHOST = "ARRR YOU A RESTLESS SOUL?",
			FIRESTARTER = "%s IS LAYING WASTE TO THE WORLD. HA HA",
}
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.WOODLEGS = 
{
            GENERIC = "Ah, greetings dear %s!",
            ATTACKER = "%s, learn to let go of old habits!",
            MURDERER = "That's it, %s! I'm taking my glasses off!",
            REVIVER = "Excellent work, %s. You're a fast learner.",
            GHOST = "Oh dear, I knew this would happen one way or another. I'll get the hearts...",
			FIRESTARTER = "Keep lighting fires if you want your legs to catch, dear.",
}
STRINGS.CHARACTERS.WOODIE.DESCRIBE.WOODLEGS = 
{
            GENERIC = "%s! Hey bud!",
            ATTACKER = "%s has raiding and looting on the brain... 'Luce, keep an eye on his legs.",
            MURDERER = "Your fancy sword versus my axe, let's see whatcha got, eh!",
            REVIVER = "%s, you're an alright guy with tasty legs, eh.",
            GHOST = "C'mon, bud, let's get you on your feet -err... Pegs.",
			FIRESTARTER = "Now what are you gonna do if your legs catch fire, eh?",
}
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.WOODLEGS = 
{
            GENERIC = "Greetings, Mr. %s.",
            ATTACKER = "Is this amusing to you, %s?",
            MURDERER = "Don't think I'll hesitate, murderous fiend!",
            REVIVER = "%s tethers lost spirits to this world.",
            GHOST = "Do you desire a heart, %s?",
			FIRESTARTER = "Is it wise to be starting fires, given your circumstance?",
}
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.WOODLEGS = 
{
	       GENERIC = "Sea deities' wisdom guide you, %s!",
	       ATTACKER = "Do not test me, as we are not so different, you and I!",
	       MURDERER = "If I must take down a Sea Viking then I shall do so! Fight!",
	       REVIVER = "%s's heart is as mighty as his beard.",
	       GHOST = "You'll not away to Valhalla without me, %s. A heart!",
	       FIRESTARTER = "%s is on the path of a tyrant raid!",
}
STRINGS.CHARACTERS.WEBBER.DESCRIBE.WOODLEGS = 
{
	       GENERIC = "Hi %s! Have you polished your legs?",
	       ATTACKER = "%s looks angry today.",
	       MURDERER = "%s don't be so salty, you're scaring us!",
	       REVIVER = "%s is scary, but nice.",
	       GHOST = "As if pirates weren't scary enough!",
	       FIRESTARTER = "Um. I think %s is setting up a fire pirate fight.",
}
STRINGS.CHARACTERS.WINONA.DESCRIBE.WOODLEGS =
{
            GENERIC = "Hey there, %s. Yarr-harr!",
            ATTACKER = "Woah! Watch those angry pirate pegs or yours, matey!",
            MURDERER = "Takin' the pirate thing too far, %s!",
            REVIVER = "That was good work there, %s. Your heart's the real treasure here!",
            GHOST = "Shake it off, %s, there's work to do!",
			FIRESTARTER = "Quit startin' fires, %s! Check your pegs!",
}

GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.LUCKYHAT = "Does it make me look scurvy... I mean scary!?"
GLOBAL.STRINGS.CHARACTERS.WILLOW.DESCRIBE.LUCKYHAT = "I don't think it was made for me."
GLOBAL.STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.LUCKYHAT = "Is head clothes."
GLOBAL.STRINGS.CHARACTERS.WENDY.DESCRIBE.LUCKYHAT = "Haughty seadog couture."
GLOBAL.STRINGS.CHARACTERS.WX78.DESCRIBE.LUCKYHAT = "BUT WHAT IS ITS PURPOSE"
GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.LUCKYHAT = "It would better suit Woodlegs than I."
GLOBAL.STRINGS.CHARACTERS.WOODIE.DESCRIBE.LUCKYHAT = "Could do with a pair of woolly earflaps."
GLOBAL.STRINGS.CHARACTERS.WAXWELL.DESCRIBE.LUCKYHAT = "Swashbuckling..."
GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.LUCKYHAT = "Pales in comparison to my helm."
GLOBAL.STRINGS.CHARACTERS.WEBBER.DESCRIBE.LUCKYHAT = "Yarr! Haha."
GLOBAL.STRINGS.CHARACTERS.WINONA.DESCRIBE.LUCKYHAT = "Do I look like a pirate to you?"
--
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.LUCKYCUTLASS = "Somebody is going to walk the plank!"
GLOBAL.STRINGS.CHARACTERS.WILLOW.DESCRIBE.LUCKYCUTLASS = "It doesn't feel lucky."
GLOBAL.STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.LUCKYCUTLASS = "Wolfgang is ready for battle!"
GLOBAL.STRINGS.CHARACTERS.WENDY.DESCRIBE.LUCKYCUTLASS = "The sword of a murderous pirate."
GLOBAL.STRINGS.CHARACTERS.WX78.DESCRIBE.LUCKYCUTLASS = "STUPID PIRATE FLESHLING HAS GIVEN ME THEIR PRIZED SLICING DEVICE"
GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.LUCKYCUTLASS = "Careful, it's sharp."
GLOBAL.STRINGS.CHARACTERS.WOODIE.DESCRIBE.LUCKYCUTLASS = "That's a pretty lucky little cutter, eh?"
GLOBAL.STRINGS.CHARACTERS.WAXWELL.DESCRIBE.LUCKYCUTLASS = "A gentleman's weapon, fit for me."
GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.LUCKYCUTLASS = "Oh great sword of the pirate, I shall swing you with honor"
GLOBAL.STRINGS.CHARACTERS.WEBBER.DESCRIBE.LUCKYCUTLASS = "Now all we need is a couple eye-patches!"
GLOBAL.STRINGS.CHARACTERS.WINONA.DESCRIBE.LUCKYCUTLASS = "Not too keen on being a pirate."

GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.WOODLEGS_BURIEDTREASURE = "Please be a good treasure!"
GLOBAL.STRINGS.CHARACTERS.WILLOW.DESCRIBE.WOODLEGS_BURIEDTREASURE = "If I don't like the treasure I'll just set the chest on fire."
GLOBAL.STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.WOODLEGS_BURIEDTREASURE = "Is mine!"
GLOBAL.STRINGS.CHARACTERS.WENDY.DESCRIBE.WOODLEGS_BURIEDTREASURE = "Dead people's stuff."
GLOBAL.STRINGS.CHARACTERS.WX78.DESCRIBE.WOODLEGS_BURIEDTREASURE = "ILLOGICAL STORAGE OF PRECIOUS OBJECTS"
GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.WOODLEGS_BURIEDTREASURE = "My curiosity is getting the better of me!"
GLOBAL.STRINGS.CHARACTERS.WOODIE.DESCRIBE.WOODLEGS_BURIEDTREASURE = "Time to get digging!"
GLOBAL.STRINGS.CHARACTERS.WAXWELL.DESCRIBE.WOODLEGS_BURIEDTREASURE = "Ah, what's this then?"
GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.WOODLEGS_BURIEDTREASURE = "May luck smile ön me this day."
GLOBAL.STRINGS.CHARACTERS.WEBBER.DESCRIBE.WOODLEGS_BURIEDTREASURE = "Treasure, treasure, treasure!"
GLOBAL.STRINGS.CHARACTERS.WINONA.DESCRIBE.WOODLEGS_BURIEDTREASURE = "Only one way to find out our luck! Let's get diggin'!"

GLOBAL.STRINGS.CHARACTERS.GENERIC.ANNOUNCE_TREASURE = "Treasure!"
GLOBAL.STRINGS.CHARACTERS.WILLOW.ANNOUNCE_TREASURE = "\"X\" marks the spot!"
GLOBAL.STRINGS.CHARACTERS.WOLFGANG.ANNOUNCE_TREASURE = "Wolfgang sees presents!"
GLOBAL.STRINGS.CHARACTERS.WENDY.ANNOUNCE_TREASURE = "Treasure... How exciting!"
GLOBAL.STRINGS.CHARACTERS.WX78.ANNOUNCE_TREASURE = "VALUABLES DETECTED NEARBY"
GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.ANNOUNCE_TREASURE = "Behold! Gold."
GLOBAL.STRINGS.CHARACTERS.WOODIE.ANNOUNCE_TREASURE = "Here comes the gold rush!"
GLOBAL.STRINGS.CHARACTERS.WAXWELL.ANNOUNCE_TREASURE = "Ah! I sense another treasure to find."
GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.ANNOUNCE_TREASURE = "I am smiled upon this day!"
GLOBAL.STRINGS.CHARACTERS.WEBBER.ANNOUNCE_TREASURE = "We spot goodies!"
GLOBAL.STRINGS.CHARACTERS.WINONA.ANNOUNCE_TREASURE = "Hear that? That's the sound of treasure plantin'! Let's go!"