local assets =
{
	Asset("ANIM", "anim/woodlegs.zip"),
	Asset("ANIM", "anim/ghost_woodlegs_build.zip"),
}

local skins =
{
	normal_skin = "woodlegs",
	ghost_skin = "ghost_woodlegs_build",
}

local base_prefab = "woodlegs"

local tags = {"WOODLEGS", "CHARACTER"}

return CreatePrefabSkin("woodlegs_none",
{
	base_prefab = base_prefab, 
	skins = skins, 
	assets = assets,
	tags = tags,
	
	skip_item_gen = true,
	skip_giftable_gen = true,
})