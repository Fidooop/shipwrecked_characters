local MakePlayerCharacter = require "prefabs/player_common"

local assets = 
{
		Asset("SCRIPT", "scripts/prefabs/player_common.lua"),

		Asset("ANIM", "anim/woodlegs.zip"),
	    Asset("ANIM", "anim/ghost_woodlegs_build.zip" ),

		Asset("SOUNDPACKAGE", "sound/woodlegs.fev"),
		Asset("SOUND", "sound/woodlegs.fsb"),
		
}

local start_inv =
{
    default =
    {
        "luckyhat", "luckycutlass", "goldnugget", "goldnugget", "goldnugget",
    },
}
for k, v in pairs(TUNING.GAMEMODE_STARTING_ITEMS) do
	start_inv[string.lower(k)] = v.WOODLEGS
end

local prefabs = FlattenTree(start_inv, true)

local function onbecamehuman(inst)
    inst.components.sanity.dapperness = -TUNING.DAPPERNESS_MED
end

local function onbecameghost(inst)
end

local function onload(inst)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
    inst:ListenForEvent("ms_becameghost", onbecameghost)

    if inst:HasTag("playerghost") then
        onbecameghost(inst)
    else
        onbecamehuman(inst)
    end
end

local function CanShaveTest(inst)
    return false, "REFUSE"
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon( "woodlegs.tex" )
	inst:AddTag("woodlegs")
	inst.soundsname = "woodlegs"
	inst:AddTag("bearded")
end

local forge_fn = function(inst)
	inst.components.itemtyperestrictions:SetRestrictions({"staves", "books"})
end

local master_postinit = function(inst)
    inst.starting_inventory = start_inv[TheNet:GetServerGameMode()] or start_inv.default
	
	if TheNet:GetServerGameMode() == "lavaarena" then
        inst.forge_fn = forge_fn
		return
    end

	inst.components.health:SetMaxHealth(150)
	inst.components.hunger:SetMax(150)
	inst.components.sanity:SetMax(120)
	--if GetModConfigData("woodlegs_sanity", modname) == "enable" then 	
    inst.components.sanity.dapperness = -TUNING.DAPPERNESS_MED

    inst:AddComponent("beard")
    inst.components.beard.canshavetest = CanShaveTest
    inst.components.beard:EnableGrowth(false)
	
	inst.OnLoad = onload
    inst.OnNewSpawn = onload
end

return MakePlayerCharacter("woodlegs", prefabs, assets, common_postinit, master_postinit, start_inv)