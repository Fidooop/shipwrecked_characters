local ctn = require("containers")

local assets =
{
    Asset("ANIM", "anim/swap_chefpack.zip"),
    Asset("ATLAS", "images/inventoryimages/chefpack.xml"),
    Asset("IMAGE", "images/inventoryimages/chefpack.tex"),
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_body", "swap_chefpack", "backpack")
    owner.AnimState:OverrideSymbol("swap_body", "swap_chefpack", "swap_body")
    inst.components.container:Open(owner)
end

local function onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_body")
    owner.AnimState:ClearOverrideSymbol("backpack")
    inst.components.container:Close(owner)
end

local function onburnt(inst)
    if inst.components.container then
        inst.components.container:DropEverything()
        inst.components.container:Close()
        inst:RemoveComponent("container")
    end

    local ash = SpawnPrefab("ash")
    ash.Transform:SetPosition(inst.Transform:GetWorldPosition())

    inst:Remove()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("chefpack")
    inst.AnimState:SetBuild("swap_chefpack")
    inst.AnimState:PlayAnimation("anim")

    inst:AddTag("backpack")
    inst:AddTag("fridge")
    inst:AddTag("nocool")

    inst.foleysound = "dontstarve/movement/foley/backpack"

    inst.entity:SetPristine()

	if not TheWorld.ismastersim then	
	inst.OnEntityReplicated = function(inst) inst.replica.container:WidgetSetup("backpack") end	
	return inst
	end

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/chefpack.xml"
    inst.components.inventoryitem.imagename = "chefpack"
	inst.components.inventoryitem.cangoincontainer = false
	
    inst.MiniMapEntity:SetIcon("spicepack.tex")

    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.BODY

    inst.components.equippable:SetOnEquip( onequip )
    inst.components.equippable:SetOnUnequip( onunequip )

    inst:AddComponent("container")
	inst.components.container:WidgetSetup("backpack")
	
    inst:AddComponent("inspectable")
	
    MakeSmallBurnable(inst)
    MakeSmallPropagator(inst)
    inst.components.burnable:SetOnBurntFn(onburnt)

    MakeHauntableLaunchAndDropFirstItem(inst)

    return inst
end

STRINGS.CHEFPACK = "Chef Pouch"
STRINGS.NAMES.CHEFPACK = "Chef Pouch"
STRINGS.RECIPE_DESC.CHEFPACK = "Freshen up your foodstuffs."

return Prefab("common/inventory/chefpack", fn, assets)