require "prefabutil"

local assets =
{
	Asset("ANIM", "anim/surfboard.zip"),
    Asset("ATLAS", "images/inventoryimages/surfboard.xml"),
    Asset("IMAGE", "images/inventoryimages/surfboard.tex"),
}

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
    MakeInventoryPhysics(inst)	
    inst.entity:AddMiniMapEntity()
	inst.AnimState:SetBank("surfboard")
	inst.AnimState:SetBuild("surfboard")
	inst.AnimState:PlayAnimation("idle")
	MakeSnowCoveredPristine(inst)
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddTag("shelter")
	inst:AddTag("surfboard")
	
    inst.MiniMapEntity:SetIcon("surfboard.tex")
	
    inst:AddComponent("inspectable")
	inst:AddComponent("useableitem")
	inst:AddComponent("surfaction")

	MakeSmallBurnable(inst, TUNING.LARGE_BURNTIME)
    MakeSmallPropagator(inst) 
	
    inst:AddComponent("fuel")
    inst.components.fuel.fuelvalue = TUNING.LARGE_FUEL

    inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(TUNING.SPEAR_USES)
    inst.components.finiteuses:SetUses(TUNING.SPEAR_USES)
    inst.components.finiteuses:SetOnFinished(inst.Remove)
	
    inst:AddComponent("inventoryitem")   
    inst.components.inventoryitem.atlasname = "images/inventoryimages/surfboard.xml"
    inst.components.inventoryitem:SetOnPickupFn(function()  end)

	MakeHauntableWork(inst)
	
	local modname = KnownModIndex:GetModActualName("Shipwrecked Characters")
	local surfboardsty = GetModConfigData("surf_sanity", modname)

	if surfboardsty == "y" then 	
	   inst:AddComponent("sanityaura")
       inst.components.sanityaura.aura = TUNING.SANITYAURA_TINY 
    end

    return inst
end

STRINGS.SURFBOARD = "Surfboard"
STRINGS.NAMES.SURFBOARD = "Surfboard"
STRINGS.RECIPE_DESC.SURFBOARD = "Cowabunga dudes!"

return Prefab( "common/inventory/surfboard", fn, assets),
	   MakePlacer( "common/surfboard_placer", "surfboard", "surfboard", "idle" )