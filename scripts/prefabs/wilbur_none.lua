local assets =
{
	Asset("ANIM", "anim/wilbur.zip"),
	Asset("ANIM", "anim/wilbur_run.zip"),
	Asset( "ANIM", "anim/ghost_wilbur_build.zip" ),
}

local skins =
{
	normal_skin = "wilbur",
	ghost_skin = "ghost_wilbur_build",
}

local base_prefab = "wilbur"

local tags = {"WILBUR", "CHARACTER"}

return CreatePrefabSkin("wilbur_none",
{
	base_prefab = base_prefab, 
	skins = skins, 
	assets = assets,
	tags = tags,
	
	skip_item_gen = true,
	skip_giftable_gen = true,
})