local MakePlayerCharacter = require "prefabs/player_common"

local assets = 
{
		Asset("SCRIPT", "scripts/prefabs/player_common.lua"),

		Asset("ANIM", "anim/wilbur.zip"),
		Asset("ANIM", "anim/wilbur_run.zip"),
		Asset("ANIM", "anim/ghost_wilbur_build.zip" ),

		Asset("SOUNDPACKAGE", "sound/wilbur.fev"),
		Asset("SOUND", "sound/wilbur.fsb"),
		
}

local start_inv =
{
    default =
    {
    "cave_banana",
	"cave_banana",
	"cave_banana",
	"cave_banana",
	"cave_banana",
	"cave_banana",
	"cave_banana",
	"cave_banana",
	"cave_banana",
	"cave_banana",
    },
}
for k, v in pairs(TUNING.GAMEMODE_STARTING_ITEMS) do
	start_inv[string.lower(k)] = v.WILBUR
end

local prefabs = FlattenTree(start_inv, true)

local function oneat(inst, food)
	if food and (food.prefab == "cave_banana" or food.prefab == "cave_banana_cooked") then
		if inst.components.sanity then
			inst.components.sanity:DoDelta(TUNING.SANITY_SMALL)
		end
	end
end

local function onbecamehuman(inst)
	inst.components.locomotor.runspeed = TUNING.WILSON_RUN_SPEED-0.5
	inst.components.hunger:SetRate(1*TUNING.WILSON_HUNGER_RATE)
	inst:AddTag("slowed")
end
local function onbecameghost(inst)
end

local function onload(inst)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
    inst:ListenForEvent("ms_becameghost", onbecameghost)

    if inst:HasTag("playerghost") then
        onbecameghost(inst)
    else
        onbecamehuman(inst)
    end
end

local forge_fn = function(inst)
        event_server_data("lavaarena", "prefabs/wilbur").master_postinit(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"staves", "books"})
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon( "wilbur.tex" )
	inst.soundsname = "wilbur"
	inst:AddTag("wilbur")
    inst:AddTag("slowed")	
end

local master_postinit = function(inst)
    inst.starting_inventory = start_inv[TheNet:GetServerGameMode()] or start_inv.default

	inst.components.health:SetMaxHealth(125)
	inst.components.hunger:SetMax(175)
	inst.components.sanity:SetMax(150)

	inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("poop")
	inst.components.periodicspawner:SetRandomTimes(TUNING.TOTAL_DAY_TIME * 2.45, TUNING.SEG_TIME * 2.2)
    inst.components.periodicspawner:SetDensityInRange(20, 2)
    inst.components.periodicspawner:SetMinimumSpacing(8)
    inst.components.periodicspawner:Start()
	
	inst.components.eater:SetOnEatFn(oneat)
	inst.components.eater.ignoresspoilage = true
	
	inst.components.hunger:SetRate(1*TUNING.WILSON_HUNGER_RATE)
	inst.components.locomotor.runspeed = TUNING.WILSON_RUN_SPEED-0.5

	inst.OnLoad = onload
	inst.OnNewSpawn = onload
	inst.timeinmotion = 0
	
	if TheNet:GetServerGameMode() == "lavaarena" then
        inst.forge_fn = forge_fn
		return
    end

    inst:ListenForEvent("locomote", function()

		inst.startpos = inst:GetPosition()
		inst.checktask = inst:DoTaskInTime(0, function()
		    inst.endpos = inst:GetPosition()
			if inst.startpos == inst.endpos then
			    inst.timeinmotion = 0
			end
			if inst.startpos ~= inst.endpos then
			    inst.timeinmotion = inst.timeinmotion + 1
			end
			if inst.timeinmotion == 0 and inst:HasTag("faster") then
        		    inst:AddTag("slowed")
        		    inst:RemoveTag("faster")
			    inst.components.hunger:SetRate(1*TUNING.WILSON_HUNGER_RATE)
			    inst.components.locomotor.runspeed = TUNING.WILSON_RUN_SPEED-0.5
			end
			if inst.timeinmotion == 60 and inst:HasTag("slowed") and not inst.components.rider:IsRiding() and not inst.components.inventory:IsHeavyLifting() then
        		    inst:AddTag("faster")
        		    inst:RemoveTag("slowed")
			    inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE * 1.33)
			    inst.components.locomotor.runspeed = TUNING.WILSON_RUN_SPEED + 2.5
			end
		end)
	end)
end

return MakePlayerCharacter("wilbur", prefabs, assets, common_postinit, master_postinit, start_inv)