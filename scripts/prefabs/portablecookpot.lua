require "prefabutil"

local FOODSTCOOK = require("preparedfoods")
local cooking = require("cooking")

local assets =
{
	Asset("ANIM", "anim/cook_pot_warly.zip"),
	Asset("IMAGE", "images/inventoryimages/portablecookpotinv.tex"),	
	Asset("ATLAS", "images/inventoryimages/portablecookpotinv.xml"),
}

local prefabs = {"collapse_small"}
for k,v in pairs(cooking.recipes.cookpot) do table.insert(prefabs, v.name) end
for k,recipe in pairs (FOODSTCOOK) do AddCookerRecipe("portablecookpot", recipe) end

local function onhit(inst, worker)
        if inst.components.stewer:IsCooking() then
            inst.AnimState:PlayAnimation("hit_cooking")
            inst.AnimState:PushAnimation("cooking_loop", true)
        elseif inst.components.stewer:IsDone() then
            inst.AnimState:PlayAnimation("hit_full")
            inst.AnimState:PushAnimation("idle_full", false)
        else
            inst.AnimState:PlayAnimation("hit_empty")
            inst.AnimState:PushAnimation("idle_empty", false)
        end
end

--anim and sound callbacks
local function startcookfn(inst)
     if not inst:HasTag("iscooking") then inst:AddTag("iscooking") end
        inst.AnimState:PlayAnimation("cooking_loop", true)
        inst.SoundEmitter:KillSound("snd")
        inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_rattle", "snd")
        inst.Light:Enable(true)
end

local function onopen(inst)
        inst.AnimState:PlayAnimation("cooking_pre_loop", true)
        inst.SoundEmitter:KillSound("snd")
        inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_open")
        inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot", "snd")
end

local function onclose(inst)
        if not inst.components.stewer:IsCooking() then
            inst.AnimState:PlayAnimation("idle_empty")
            inst.SoundEmitter:KillSound("snd")
        end
        inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_close")
end

local function spoilfn(inst)
        inst.components.stewer.product = inst.components.stewer.spoiledproduct
        inst.AnimState:OverrideSymbol("swap_cooked", "cook_pot_food", inst.components.stewer.product)
end

local function ShowProduct(inst)
        local product = inst.components.stewer.product
        if IsModCookingProduct(inst.prefab, product) then
            inst.AnimState:OverrideSymbol("swap_cooked", product, product)
        else
            inst.AnimState:OverrideSymbol("swap_cooked", "cook_pot_food", product)
        end
end

local function donecookfn(inst)
     if inst:HasTag("iscooking") then inst:RemoveTag("iscooking") end
        inst.AnimState:PlayAnimation("cooking_pst")
        inst.AnimState:PushAnimation("idle_full", false)
        ShowProduct(inst)
        inst.SoundEmitter:KillSound("snd")
        inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish")
        inst.Light:Enable(false)
end

local function continuedonefn(inst)
        inst.AnimState:PlayAnimation("idle_full")
        ShowProduct(inst)
end

local function continuecookfn(inst)
        inst.AnimState:PlayAnimation("cooking_loop", true)
        inst.Light:Enable(true)
        inst.SoundEmitter:KillSound("snd")
        inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_rattle", "snd")
end

local function harvestfn(inst)
     if inst:HasTag("iscooking") then inst:RemoveTag("iscooking") end
        inst.AnimState:PlayAnimation("idle_empty")
end

local function onfar(inst)
    if inst.components.container ~= nil then
        inst.components.container:Close()
    end
end

local function getstatus(inst)
    return (inst:HasTag("burnt") and "BURNT")
        or (inst.components.stewer:IsDone() and "DONE")
        or (not inst.components.stewer:IsCooking() and "EMPTY")
        or (inst.components.stewer:GetTimeToCook() > 15 and "COOKING_LONG")
        or "COOKING_SHORT"
end

local function OnHaunt(inst, haunter)
    local ret = false
    if inst.components.stewer ~= nil and
        inst.components.stewer.product ~= "wetgoop" and
        math.random() <= TUNING.HAUNT_CHANCE_ALWAYS then
        if inst.components.stewer:IsCooking() then
            inst.components.stewer.product = "wetgoop"
            inst.components.hauntable.hauntvalue = TUNING.HAUNT_MEDIUM
            ret = true
        elseif inst.components.stewer:IsDone() then
            inst.components.stewer.product = "wetgoop"
            inst.components.hauntable.hauntvalue = TUNING.HAUNT_MEDIUM
            continuedonefn(inst)
            ret = true
        end
    end
    return ret
end

local function onsavescp(inst, data)
    if inst.components.container ~= nil then inst.components.container:Close() end
end
local function onloadscp(inst, data)
	if inst.components.container ~= nil then inst.components.container:Close() end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle_empty", false)
    inst.SoundEmitter:PlaySound("dontstarve/common/cook_pot_craft")
end

local function ondeploy(inst, pt, deployer) 
    local cpot = SpawnPrefab("portablecookpot")
    if cpot ~= nil then 
		cpot.Physics:SetCollides(false)	
        cpot.Physics:Teleport(pt.x, 0, pt.z)		
		cpot.Physics:SetCollides(true)	
		cpot:syncanim("place", true)
        cpot:syncanimpush("idle_empty", false)
		cpot.SoundEmitter:PlaySound("dontstarve/common/place_structure_stone")
		inst:Remove()
    end 
end

local function syncanim(inst, animname, loop)
    inst.AnimState:PlayAnimation(animname, loop)
    inst.AnimState:PlayAnimation(animname, loop)
end

local function syncanimpush(inst, animname, loop)
    inst.AnimState:PushAnimation(animname, loop)
    inst.AnimState:PushAnimation(animname, loop)
end

local function pickuppot(inst, guy)
	if guy.components and guy.components.inventory then
		local crockpotitem = SpawnPrefab("portablecookpot_item")
		guy.components.inventory:GiveItem(crockpotitem)
		inst:Remove()
		return true
	end
end

local function item_droppedfn(inst)
	--If this is a valid place to be deployed, auto deploy yourself.
	if inst.components.deployable and inst.components.deployable:CanDeploy(inst:GetPosition()) then
		inst.components.deployable:Deploy(inst:GetPosition(), inst)
	end
end

local function itemfn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
    inst.entity:AddMiniMapEntity()
    MakeInventoryPhysics(inst)	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.MiniMapEntity:SetIcon("cookpotwarly.tex")

	inst.AnimState:SetBank("cook_pot_warly")
	inst.AnimState:SetBuild("cook_pot_warly")
	inst.AnimState:PlayAnimation("idle_drop")
	
    inst:AddComponent("inspectable")
	
    inst:AddComponent("inventoryitem")   
	inst.components.inventoryitem.imagename = "portablecookpotinv"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/portablecookpotinv.xml"
    inst.components.inventoryitem:SetOnPickupFn(function()  end)

	MakeHauntableWork(inst)

	inst:AddComponent("deployable")
	inst.components.deployable.ondeploy = ondeploy
	inst.components.deployable.placer = "portablecookpot_placer"
	
	return inst
end

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddLight()
	inst.entity:AddNetwork()
	inst.entity:AddMiniMapEntity()
	
    MakeInventoryPhysics(inst)	
	MakeObstaclePhysics(inst, .5)
	
	inst.MiniMapEntity:SetIcon("cookpotwarly.tex")
	
	inst.Light:Enable(false)
    inst.Light:SetRadius(.6)
    inst.Light:SetFalloff(1)
    inst.Light:SetIntensity(.5)
    inst.Light:SetColour(235/255,62/255,12/255)

	inst:AddTag("structure")
	
	inst.AnimState:SetBank("cook_pot_warly")
	inst.AnimState:SetBuild("cook_pot_warly")
	inst.AnimState:PlayAnimation("idle_empty")
	
	MakeSnowCoveredPristine(inst)
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then	
	inst.OnEntityReplicated = function(inst) inst.replica.container:WidgetSetup("cookpot") end	
	return inst
	end
	
	inst.syncanim = syncanim
    inst.syncanimpush = syncanimpush
	
	inst:AddComponent("stewer")
    inst.components.stewer.onstartcooking = startcookfn
    inst.components.stewer.oncontinuecooking = continuecookfn
    inst.components.stewer.oncontinuedone = continuedonefn
    inst.components.stewer.ondonecooking = donecookfn
    inst.components.stewer.onharvest = harvestfn
    inst.components.stewer.onspoil = spoilfn
	
    inst:AddComponent("container")
    inst.components.container:WidgetSetup("cookpot")
    inst.components.container.onopenfn = onopen
    inst.components.container.onclosefn = onclose
	inst:ListenForEvent("itemget", function(inst, data)
		if inst.components.container ~= nil then
			inst.components.container:Open(ThePlayer)
		end
	end)
	
	inst:AddComponent("inspectable")
	inst.components.inspectable.getstatus = getstatus
	
	inst:AddComponent("playerprox")
	inst.components.playerprox:SetDist(3,5)
	inst.components.playerprox:SetOnPlayerFar(onfar)
	
	inst:AddComponent("hauntable")
    inst.components.hauntable:SetOnHauntFn(OnHaunt)

	inst:AddComponent("getitem")
	inst.components.getitem:SetOnPickupFn(pickuppot)
	
	MakeSnowCovered(inst)
    inst:ListenForEvent("onbuilt", onbuilt)
	
	inst.OnSave = onsavescp
    inst.OnLoad = onloadscp
	
    return inst
end

STRINGS.PORTABLECOOKPOT = "Portable Crock Pot"
STRINGS.NAMES.PORTABLECOOKPOT = "Portable Crock Pot"
STRINGS.NAMES.PORTABLECOOKPOT_ITEM = "Portable Crock Pot"
STRINGS.RECIPE_DESC.PORTABLECOOKPOT_ITEM = "Your cooking pal to go!"

return Prefab( "common/objects/portablecookpot", fn, assets, prefabs),
    MakePlacer( "common/portablecookpot_placer", "cook_pot_warly", "cook_pot_warly", "idle_empty"),
	MakePlacer( "common/portablecookpot_item_placer", "cook_pot_warly", "cook_pot_warly", "idle_empty"),
	Prefab("common/portablecookpot_item", itemfn, assets, prefabs)