local MakePlayerCharacter = require "prefabs/player_common"

local assets = 
{
		Asset("SCRIPT", "scripts/prefabs/player_common.lua"),
		
		Asset("ANIM", "anim/walani.zip"),
	    Asset("ANIM", "anim/ghost_walani_build.zip" ),

		Asset("SOUNDPACKAGE", "sound/walani.fev"),
		Asset("SOUND", "sound/walani.fsb"),
		
}

local start_inv =
{
    default =
    {
        "surfboard",
    },
}
for k, v in pairs(TUNING.GAMEMODE_STARTING_ITEMS) do
	start_inv[string.lower(k)] = v.WALANI
end

local prefabs = FlattenTree(start_inv, true)
local easing = require("easing")

local function NoWetnessRate(inst)
	local moist_dapperness = 0
	for k, v in pairs(inst.components.inventory.equipslots) do
		if v:GetIsWet() and v.components.equippable then
			moist_dapperness = moist_dapperness - TUNING.WET_ITEM_DAPPERNESS
		end
	end
	local moisture = inst.components.moisture
	local moisture_delta = easing.inSine(moisture:GetMoisture(), 0, TUNING.MOISTURE_SANITY_PENALTY_MAX, moisture:GetMaxMoisture())
	return moist_dapperness - moisture_delta
end

local function onbecamehuman(inst)
	inst.components.sanity.custom_rate_fn = NoWetnessRate
	inst.components.moisture.baseDryingRate = 0.3
	inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE * 1.10)
	inst.components.sanity.sanityrate = -0.1
end
local function onbecameghost(inst)
   inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "walani_walk")
end

local function onload(inst)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
    inst:ListenForEvent("ms_becameghost", onbecameghost)

    if inst:HasTag("playerghost") then
        onbecameghost(inst)
    else
        onbecamehuman(inst)
    end
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon( "walani.tex" )
    inst:AddTag("walani")
	inst.soundsname = "walani"
end

local forge_fn = function(inst)
        event_server_data("lavaarena", "prefabs/walani").master_postinit(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "staves", "books"})
		inst.components.revivablecorpse:SetReviveSpeedMult(1.5) --change #
end

local master_postinit = function(inst)
    inst.starting_inventory = start_inv[TheNet:GetServerGameMode()] or start_inv.default

	inst.components.health:SetMaxHealth(120)
	inst.components.hunger:SetMax(200)
	inst.components.sanity:SetMax(200)
	
	inst.components.sanity.custom_rate_fn = NoWetnessRate
	
	inst.components.moisture.baseDryingRate = 0.3
	inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE * 1.10)
	inst.components.sanity.sanityrate = -0.1
	
	if TheNet:GetServerGameMode() == "lavaarena" then
        inst.forge_fn = forge_fn
		return
    end
	
	inst.OnLoad = onload
    inst.OnNewSpawn = onload
end

return MakePlayerCharacter("walani", prefabs, assets, common_postinit, master_postinit, start_inv)