local assets =
{
    Asset("ANIM", "anim/xmarksspot.zip"),
}
local prefabs =
{
}

local woodlegsloothaul={
	loots1 = --2,10
	{
	    cutgrass = 5,
	    twigs = 4,
	    cutreeds = 1,
	    rocks = 4,
	    flint = 2,
	    nitre = 2,
		papyrus = 1,
		goldnugget = 3,
		gears = 1,
		rope =1,
		transistor =1,
		torch = 1,
		charcoal = 2,
		ash = 2,
		poop = 2,
		petals = 3,
		flowerhat = 1,
		red_cap= 1,
		green_cap = 1,
		blue_cap = 1,
	},
	
	loots2 = --1,3
	{	       
	    gears = 3,
	    thulecite = 2,
	    tentaclespots = 1,
	    slurper_pelt = 1,
	    lightninggoathorn = 1,
	    walrus_tusk = 1,
	    steelwool = 1,
	    redgem = 1,
	    bluegem = 1,
	    purplegem = 1,
		healingsalve =3,
		blowdart_sleep = 2,
		goldnugget = 3,
		bearger_fur = 1,
	},
	loots3 = --1 only
	{	
	    footballhat = 1,
		armorwood = 1,
		spear = 1,
		goldenaxe = 1,
	    goldenshovel = 1,
		goldenpickaxe= 1,
	    cane = 1,
		boomerang = 1,
		strawhat = 1,
	    armormarble = 1,
	    panflute = 1,
		ruins_bat = 1,
	    ruinshat = 1,
	    armorruins = 1,
	    armordragonfly = 1,	
		nightsword = 1,
		tophat = 1,
		amulet = 1,
		reviver = 1,
		lifeinjector = 1,
	}, 

	loots4 = --1,7
	{
	    
	    goldnugget = 3,
	    log = 3,
		transistor = 2,
	    beardhair = 1,
		blowdart_fire = 1,
		nightmarefuel = 1,
	    feather_crow = 1,
	    feather_canary = 1,
		DRESS_blueprint = 1,
		axe = 1,
		hammer = 1,
		shovel = 1,
		flint = 3,
		moonrocknugget = 1,
		boneshard = 3,
		nightmarefuel = 1,
		jellybean = 1,
	},
	
	loots5 = --1,2
	{	    
	   redgem = 1,
	   bluegem = 1,
	   purplegem = 1, 
	   purplegem = .5,
	   orangegem = .25,
	   yellowgem = .25,
	   greengem = .25,
	   transistor = 1,
	   gunpowder = 1,
	   heatrock = 1,
	   TOOLS_blueprint = 1,	
	   trap_teeth = 1,
       hambat = 1,	
       icestaff = 1,
       goldnugget = 2,	   
	},
	
	loots6 = --1,6
	{
	    blowdart_pipe = 2,
	    thulecite_pieces = .5,
	    pigskin = 1,
	    REFINE_blueprint = 1,	
	    goose_feather = 1,
	    nightmarefuel = 1,
		compass = 1,
		boneshard = 2,
		ice = 3,
		hawaiianshirt = 1,
		umbrella = 1,
		beehat = 1,
		bandage = 1,
		honey = 2,
		townportaltalisman = 1,
	    redgem = 1,
	    bluegem = 1,
		goldnugget = 1,	
	},
}

local MOBS =
{
    ghost = 1, --Numbers mean nothing when spawning mobs
    spider = 1,
    spider = 3,
    mosquito = 4,
    hound = 2,
	beeguard = 2,
	spider_warrior = 2,
	killerbee = 1,
	monkey = 2,
}

local function dospawntchest(inst,doer)
    local chest = SpawnPrefab("treasurechest")
    local x, y, z = inst.Transform:GetWorldPosition()
    chest.Transform:SetPosition(x, 0, z)
    for k, v in pairs(woodlegsloothaul) do		
	local item = nil
	local itemgive = false	
	local itemnum = 1	

	if     k == "loots1" then
		itemgive = true
		itemnum = math.random(2,10)
	elseif k == "loots2" then
		itemgive = true
		itemnum = math.random(1,3)
	elseif k == "loots3" then
		itemgive = true		
	elseif k == "loots4" then
		itemgive = true
		itemnum = math.random(1,7)
	elseif k == "loots5" then
		itemgive = true
		itemnum = math.random(1,2)
    elseif k == "loots6" then
		itemgive = true
		itemnum = math.random(1,6)
	end

	if itemgive then
		item =  SpawnPrefab(weighted_random_choice(v) )	
		if item.components.stackable then
			item.components.stackable:SetStackSize(itemnum)
		end
		chest.components.container:GiveItem(item)
	end
    end
end

local function spawnhostile(inst, chance)
    if inst.ghost == nil and math.random() <= (chance or 4) then
	local mobsurprise = weighted_random_choice(MOBS)
--	local mobcount = weighted_random_choice(MOBNUM)
        inst.ghost = SpawnPrefab(mobsurprise)
        if inst.ghost ~= nil then
            local x, y, z = inst.Transform:GetWorldPosition()
            inst.ghost.Transform:SetPosition(x - .3, y, z - .3)
            inst:ListenForEvent("onremove", function() inst.ghost = nil end, inst.ghost)
            return true
        end
    end
    return false
end

local function onfinishcallback(inst, worker)
    inst:RemoveComponent("workable")

	    if worker ~= nil then
        if worker.components.sanity ~= nil and not worker:HasTag("woodlegs") then
            worker.components.sanity:DoDelta(-TUNING.SANITY_TINY) -- -5 Sanity
        end
        if not spawnhostile(inst, .3) then --30% 
        --  local item = math.random() < .5 and PickRandomTrinket() or weighted_random_choice(LOOTS) or nil
		    local cpt = Vector3(inst.Transform:GetWorldPosition())
           SpawnPrefab("small_puff").Transform:SetPosition(cpt:Get())  
           dospawntchest(inst,worker)
           inst:Remove()
        end
    end	
--		inst.SoundEmitter:PlaySound("dontstarve/characters/wx78/levelup", "sound")
		inst:Remove()
end

local function OnSave(inst, data)
    if inst.components.workable == nil then
	   inst:Remove()
    end
end

local function OnLoad(inst, data)
    if data ~= nil and data.dug or inst.components.workable == nil then
        inst:RemoveComponent("workable")
        inst:Remove()
    end
end

local function fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
	inst.entity:AddMiniMapEntity()
	inst.entity:AddSoundEmitter()
	
    inst.MiniMapEntity:SetIcon("woodlegsxspot.tex")

    inst.AnimState:SetBank("x_marks_spot")
    inst.AnimState:SetBuild("x_marks_spot")
    inst.AnimState:PlayAnimation("anim")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

	inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.DIG)
    inst.components.workable:SetWorkLeft(1)
	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLoot({"boneshard"})
	
	inst.components.workable:SetOnFinishCallback(onfinishcallback)
	
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    return inst
end

STRINGS.WOODLEGS_BURIEDTREASURE = "X Marks The Spot"
STRINGS.NAMES.WOODLEGS_BURIEDTREASURE = "X Marks The Spot"

return Prefab("woodlegs_buriedtreasure", fn, assets, prefabs)