local assets =
{
	Asset("ANIM", "anim/luckycutlass.zip"),
	Asset("ANIM", "anim/swap_luckycutlass.zip"),
	Asset("ATLAS", "images/inventoryimages/luckycutlass.xml"),
    Asset("IMAGE", "images/inventoryimages/luckycutlass.tex"),
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_luckycutlass", "luckycutlass")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function onattack(inst, attacker, target)
    if attacker:HasTag("luckylegs") then
    inst.components.weapon:SetDamage(TUNING.BATBAT_DAMAGE*1.25) 
    else
	inst.components.weapon:SetDamage(TUNING.BATBAT_DAMAGE)
    end
end

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("luckycutlass")
    inst.AnimState:SetBuild("luckycutlass")
    inst.AnimState:PlayAnimation("idle")

	inst:AddTag("sharp")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.BATBAT_DAMAGE)
	inst.components.weapon:SetOnAttack(onattack)
	
	inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(TUNING.SPEAR_USES)
    inst.components.finiteuses:SetUses(TUNING.SPEAR_USES)

    inst.components.finiteuses:SetOnFinished(inst.Remove)
	
	MakeHauntableLaunch(inst)
	
    inst.entity:SetPristine()
	
    inst:AddComponent("inspectable")
	
    inst:AddComponent("equippable")
	inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "luckycutlass"
	inst.components.inventoryitem.atlasname = "images/inventoryimages/luckycutlass.xml"
	
    return inst
end

STRINGS.LUCKYCUTLASS = "Lucky Cutlass"
STRINGS.NAMES.LUCKYCUTLASS = "Lucky Cutlass"
STRINGS.RECIPE_DESC.LUCKYCUTLASS = "Master of cutting and lass. Cutlass."

return Prefab("common/inventory/luckycutlass", fn, assets)