local MakePlayerCharacter = require "prefabs/player_common"

local assets = 
{
		Asset("SCRIPT", "scripts/prefabs/player_common.lua"),
		
		Asset("ANIM", "anim/warly.zip"),
		Asset("ANIM", "anim/cook_pot_warly.zip"),
        Asset("ANIM", "anim/ghost_warly_build.zip" ),

		Asset("SOUNDPACKAGE", "sound/warly.fev"),
		Asset("SOUND", "sound/warly.fsb"),
		
}

local start_inv =
{
    default =
    {
        "portablecookpot_item",
    },
}
for k, v in pairs(TUNING.GAMEMODE_STARTING_ITEMS) do
	start_inv[string.lower(k)] = v.WARLY
end
local prefabs = FlattenTree(start_inv, true)

local function refresh_consumed_foods(inst)
	local to_remove = {}
	for k,v in pairs(inst.consumed_foods) do
		if GetTime() >= v.time_of_reset then
			table.insert(to_remove, k)
		end
	end

	for k,v in pairs(to_remove) do
		inst.consumed_foods[v] = nil
	end
end

local function onbecamehuman(inst)
	inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE * 1.33)
end

local function onbecameghost(inst)
   inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "warly_walk")
end

local function OnSave(inst, data)
	local consumed_foods = {}
	for k,v in pairs(inst.consumed_foods) do
		consumed_foods[k] = {}
		consumed_foods[k].count = v.count
		consumed_foods[k].time_of_reset = v.time_of_reset - GetTime()
	end
	data.consumed_foods = consumed_foods
end

local function onload(inst, data)

	if data and data.consumed_foods then
		inst.consumed_foods = data.consumed_foods
	end
	
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
    inst:ListenForEvent("ms_becameghost", onbecameghost)

    if inst:HasTag("playerghost") then
        onbecameghost(inst)
    else
        onbecamehuman(inst)
    end
end

local function equipewarlychefpack(inst)
	local warlychefpack = SpawnPrefab("chefpack")    
	inst.components.inventory:Equip(warlychefpack)
end

local function getmultfn(inst, food, original_value)
	local mult = 1
	if food.components.edible then
		if food:HasTag("preparedfood") then
			mult = 1.33
		elseif food.prefab:find("cooked") then
			mult = 0.9
		elseif food.prefab:find("dried") then
			mult = 0.8
		elseif not food.prefab:find("dried") and not food.prefab:find("cooked") then
			mult = 0.7
		end
	end
	
	if inst.inst.consumed_foods[food.prefab] ~= nil and inst.inst.consumed_foods[food.prefab].count > 0 then
		local WARLY_MULT_SAME_OLD = {0.9, 0.8, 0.65, 0.5, 0.3}
		local penalty_stage = inst.inst.consumed_foods[food.prefab].count
		penalty_stage = math.clamp(penalty_stage, 1, 5)
		mult = mult * WARLY_MULT_SAME_OLD[penalty_stage]
	end
		
	if original_value < 0 then 
		mult = 1 + (1 - mult)
	end
		
	return mult
end

local specialtyfoods =
{
	monstertartare = true,
	freshfruitcrepes = true
}

local function OnEat(inst, food)
	refresh_consumed_foods(inst)
	
	local foodprefab = food.prefab
	
	if food and food.components.edible and food.components.perishable and food.components.perishable:IsFresh() 
		     and not food:HasTag("monstermeat") then
		local foodtype = ""
			
		if foodprefab == "wetgoop" then
			foodtype = "PAINFUL"			
		elseif inst.consumed_foods[foodprefab] ~= nil then
			local penalty_stage = inst.consumed_foods[foodprefab].count
			penalty_stage = math.clamp(penalty_stage, 1, 5)
			foodtype = "SAME_OLD_"..penalty_stage
		elseif specialtyfoods[foodprefab] then		
			foodtype = "TASTY"				
		else
			if food:HasTag("preparedfood") then
				foodtype = "PREPARED"
			elseif foodprefab:find("cooked") then
				foodtype = "COOKED"
			elseif foodprefab:find("dried") then
				foodtype = "DRIED"
			elseif not foodprefab:find("dried") and not foodprefab:find("cooked") then
				foodtype = "RAW"
			end
		end	
		if foodtype ~= "" then
			inst.components.talker:Say(GetString(inst.prefab, "ANNOUNCE_EAT", string.upper(foodtype)))
		end	
	end

	local WARLY_SAME_OLD_COOLDOWN = TUNING.TOTAL_DAY_TIME * 1.75
	if inst.consumed_foods[foodprefab] ~= nil then
		inst.consumed_foods[foodprefab].count = inst.consumed_foods[foodprefab].count + 1
		inst.consumed_foods[foodprefab].time_of_reset = GetTime() + (WARLY_SAME_OLD_COOLDOWN)
	else
		inst.consumed_foods[foodprefab] = {count = 0, time_of_reset = GetTime() + (WARLY_SAME_OLD_COOLDOWN)}
	end
	
end

local function LongUpdate(inst, dt)
	for k,v in pairs(inst.consumed_foods) do
		v.time_of_reset = v.time_of_reset - dt
	end
	refresh_consumed_foods(inst)
end

local forge_fn = function(inst)
        event_server_data("lavaarena", "prefabs/warly").master_postinit(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "books"})
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon( "warly.tex" )
	inst.soundsname = "warly"
	inst:AddTag("warly")
	inst:AddTag("expertchef")
end

local master_postinit = function(inst)
    inst.starting_inventory = start_inv[TheNet:GetServerGameMode()] or start_inv.default

	inst.components.health:SetMaxHealth(150)
	inst.components.hunger:SetMax(250)
	inst.components.sanity:SetMax(200)

	inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE * 1.33)
	
	inst.components.eater.getsanitymultfn = getmultfn --Not in dst, cheat used
	inst.components.eater.gethungermultfn = getmultfn
	inst.components.eater.gethealthmultfn = getmultfn

	local _Eat = inst.components.eater.Eat
	inst.components.eater.Eat = function(self, food)
		OnEat(self.inst, food)
		return _Eat(self, food)
	end
	
	inst.consumed_foods = {}

	if TheNet:GetServerGameMode() == "lavaarena" then
        inst.forge_fn = forge_fn
		return
    end
	
	inst.OnSave = OnSave	
	inst.OnLongUpdate = LongUpdate	
	inst.OnLoad = onload
    inst.OnNewSpawn = equipewarlychefpack
end

return MakePlayerCharacter("warly", prefabs, assets, common_postinit, master_postinit, start_inv)