local assets =
{
	Asset("ANIM", "anim/walani.zip"),
	Asset( "ANIM", "anim/ghost_walani_build.zip" ),
}

local skins =
{
	normal_skin = "walani",
	ghost_skin = "ghost_walani_build",
}

local base_prefab = "walani"

local tags = {"WALANI", "CHARACTER"}

return CreatePrefabSkin("walani_none",
{
	base_prefab = base_prefab, 
	skins = skins, 
	assets = assets,
	tags = tags,
	
	skip_item_gen = true,
	skip_giftable_gen = true,
})