local assets = 
{
	Asset("ANIM", "anim/hat_woodlegs.zip"),

	Asset("ATLAS", "images/inventoryimages/luckyhat.xml"),
	Asset("IMAGE", "images/inventoryimages/luckyhat.tex"),
}

local min_radius = 3
local max_radius = 15

local function woodlegs_dstloot(newsection, oldsection, inst)
    local equippable = inst.components.equippable
    if equippable ~= nil and equippable:IsEquipped() then
        local owner = inst.components.inventoryitem ~= nil and inst.components.inventoryitem.owner or nil

        if owner ~= nil and owner:HasTag("player") then

	    local pos = inst:GetPosition()
	    local offset = FindWalkableOffset(pos, math.random() * 2 * math.pi, math.random(15, 20), 18) --(25, 30), 18) in singleplayer
	    local woodlegsget = SpawnPrefab("woodlegs_buriedtreasure")
	    if offset == nil then return end

	    pos.x = pos.x + GetRandomMinMax(min_radius, max_radius) * (math.random() > 0.5 and 1 or -1)
	    pos.z = pos.z + GetRandomMinMax(min_radius, max_radius) *  (math.random() > 0.5 and 1 or -1)
	    pos.x = pos.x + offset.x
	    pos.z = pos.z + offset.z


           woodlegsget.Transform:SetPosition(pos:Get())
	       local fx = SpawnPrefab("lucy_transform_fx")
    	   fx.Transform:SetPosition(pos:Get())
	       fx.persists = false
	       owner.components.talker:Say(GetString(owner, "ANNOUNCE_TREASURE"))
		   local x, y, z = woodlegsget.Transform:GetWorldPosition()
		   if not woodlegsget:IsOnValidGround() or TheWorld.Map:IsPointNearHole(Vector3(x, 0, z)) then
		      SpawnPrefab("splash_ocean").Transform:SetPosition(x, y, z)
		      woodlegsget.Transform:SetPosition(FindSafeSpawnLocation(x, y, z))
           end
		end
    end
end

local function OnEquip(inst, owner, phase)
    owner.AnimState:OverrideSymbol("swap_hat", "hat_woodlegs", "swap_hat")
    owner.AnimState:Show("HAT")
    owner.AnimState:Show("HAIR_HAT")
    owner.AnimState:Hide("HAIR_NOHAT")
    owner.AnimState:Hide("HAIR")
        
    if owner:HasTag("player") then
	owner.AnimState:Hide("HEAD")
	owner.AnimState:Show("HEAD_HAT")
    end
	
    if inst.components.fueled then
       inst.components.fueled:StartConsuming()        
    end
	
    if owner:HasTag("woodlegs") then
	owner:AddTag("luckylegs")
    end
end
	
local function OnUnequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_hat")
    owner.AnimState:Hide("HAT")
    owner.AnimState:Hide("HAIR_HAT")
    owner.AnimState:Show("HAIR_NOHAT")
    owner.AnimState:Show("HAIR")

    if owner:HasTag("player") then
        owner.AnimState:Show("HEAD")
        owner.AnimState:Hide("HEAD_HAT")
    end

    if inst.components.fueled then
       inst.components.fueled:StopConsuming()        
    end
	
    if owner:HasTag("woodlegs") then
	owner:RemoveTag("luckylegs")
    end
end

local function fn()
    local inst = CreateEntity()
    
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("woodlegshat")
    inst.AnimState:SetBuild("hat_woodlegs")
    inst.AnimState:PlayAnimation("anim")

    inst:AddTag("hat")
		
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
    inst:AddComponent("fueled") 
    inst.components.fueled.fueltype = FUELTYPE.USAGE
    inst.components.fueled:InitializeFuelLevel(TUNING.WINTERHAT_PERISHTIME)
    inst.components.fueled:SetDepletedFn(inst.Remove)
    inst.components.fueled:SetSections(6) --8 previously
    inst.components.fueled:SetSectionCallback(woodlegs_dstloot)
	
    inst:AddComponent("inspectable")
    inst:AddComponent("lootdropper")	
    inst:AddComponent("tradable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "luckyhat"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/luckyhat.xml"
    
    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.HEAD
    inst.components.equippable:SetOnEquip(OnEquip)
    inst.components.equippable:SetOnUnequip(OnUnequip)

    MakeHauntableLaunch(inst)

    return inst
end

STRINGS.LUCKYHAT = "Lucky Hat"
STRINGS.NAMES.LUCKYHAT = "Lucky Hat"
STRINGS.RECIPE_DESC.LUCKYHAT = "Sniffs out treasures."

return Prefab("common/inventory/luckyhat", fn, assets, prefabs)