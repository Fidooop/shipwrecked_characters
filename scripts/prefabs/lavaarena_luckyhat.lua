local assets =  {
	Asset("ANIM", "anim/hat_woodlegs.zip"),

	Asset("ATLAS", "images/inventoryimages/luckyhat.xml"),
	Asset("IMAGE", "images/inventoryimages/luckyhat.tex"),
}

local function onequip(inst, owner, phase)
    owner.AnimState:OverrideSymbol("swap_hat", "hat_woodlegs", "swap_hat")
    owner.AnimState:Show("HAT")
    owner.AnimState:Show("HAIR_HAT")
    owner.AnimState:Hide("HAIR_NOHAT")
    owner.AnimState:Hide("HAIR")
    
	owner.AnimState:Hide("HEAD")
	owner.AnimState:Show("HEAD_HAT")
	
	owner:AddComponent("passive_increasingdamage")
end
	
local function onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_hat")
    owner.AnimState:Hide("HAT")
    owner.AnimState:Hide("HAIR_HAT")
    owner.AnimState:Show("HAIR_NOHAT")
    owner.AnimState:Show("HAIR")
	
	owner.AnimState:Show("HEAD")
    owner.AnimState:Hide("HEAD_HAT")
	
	owner:RemoveComponent("passive_increasingdamage")
end

local function fn()
    local inst = CreateEntity()
    
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("woodlegshat")
    inst.AnimState:SetBuild("hat_woodlegs")
    inst.AnimState:PlayAnimation("anim")

    inst:AddTag("hat")
	
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "luckyhat"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/luckyhat.xml"
	
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType("hats")
	inst.components.itemtype:SetCharacterSpecific("woodlegs")
	
	inst:AddComponent("inspectable")

	inst:AddComponent("equippable")
	inst.components.equippable.equipslot = EQUIPSLOTS.HEAD
	inst.components.equippable:SetOnEquip(onequip)
	inst.components.equippable:SetOnUnequip(onunequip)
	inst.components.equippable:AddDamageBuff(false, "luckyhat", function(attacker, victim, weapon, stimuli)
		return 0.05 * attacker.components.passive_increasingdamage.dmg_lvl
	end)

    return inst
end

return ForgePrefab("lavaarena_luckyhat", fn, assets, nil, nil, "ARMOR", true, "images/inventoryimages/luckyhat.xml", "luckyhat.tex",
		{atlas = "images/inventoryimages/luckyhat.xml", image = "luckyhat.tex"}, TUNING.FORGE.LUCKYHAT, nil, "hat_woodlegs", "common_head1")