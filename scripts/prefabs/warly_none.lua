local assets =
{
	Asset( "ANIM", "anim/warly.zip" ),
	Asset( "ANIM", "anim/ghost_warly_build.zip" ),
}

local skins =
{
	normal_skin = "warly",
	ghost_skin = "ghost_warly_build",
}

local base_prefab = "warly"

local tags = {"WARLY", "CHARACTER"}

return CreatePrefabSkin("warly_none",
{
	base_prefab = base_prefab, 
	skins = skins, 
	assets = assets,
	tags = tags,
	
	skip_item_gen = true,
	skip_giftable_gen = true,
})