return
{
	DESCRIBE_TOODARK = "Eee! Eee! Eee!",
	DESCRIBE = 
{
		CAVE_BANANA = "'Nanas!",
		CAVE_BANANA_COOKED = "'Nanas!",
		CAVE_BANANA_TREE = "'Nanas!",
		CAVE_BANANA_BURNT = "'Nonos!",
--		BANANAPOP = "'Nonos!",
			
-- Wilbur describing players so they don't use Wilson's as a default.
PLAYER = 
{
	GENERIC = "Ooo ooa oah ah!",
	ATTACKER = "Oooae Ooh?",
	MURDERER = "Ooao Ooae Oa Oaooe Ooh!",
	REVIVER = "Oao Ooooae!",
	GHOST = "Ooo, Oooae Oo! Ooo Oaao.",
	FIRESTARTER = "Ooe Ooa? Oaoh. Ooooe. Oaoe!",
},

},

}