local WalaniSurfer = Class(function(self, inst)
    self.inst = inst
    self.boundEntrance = nil
end)

local function Appear(doer)
   doer:Hide()
   doer.sg:GoToState("jumpout")
        doer:DoTaskInTime(1.3, doer.components.talker:Say(GetString(doer, "ANNOUNCE_WAVE_BOOST"))) -- for wisecracker
		SpawnPrefab("splash_ocean").Transform:SetPosition(doer.Transform:GetWorldPosition())
		doer.SoundEmitter:PlaySound("dontstarve/creatures/pengull/splash")
end

local function TeleportTime(doer)
local x, y, z = doer.Transform:GetWorldPosition()
local pt = doer:GetPosition()
local portal = TheSim:FindEntities(pt.x,pt.y,pt.z, 30000, nil, nil, {"watersource"})
      if portal ~= nil then	
         for i,v in ipairs(portal) do
                    local tp_pos = v:GetPosition() 
                    doer.Physics:Teleport(tp_pos:Get())
             doer:SnapCamera()
             doer:ScreenFade(true, 2)
         end	 
    end
end

function WalaniSurfer:TeleportAction(doer)
  doer:ScreenFade(false)

    doer:DoTaskInTime(3, Appear)
	SpawnPrefab("splash_ocean").Transform:SetPosition(doer.Transform:GetWorldPosition())
	doer.SoundEmitter:PlaySound("dontstarve/creatures/pengull/splash")

   doer.AnimState:PlayAnimation("jump")
   doer.sg:GoToState("forcetele")
   doer:Show()
   doer:DoTaskInTime(1.3, TeleportTime)	   
end

return WalaniSurfer