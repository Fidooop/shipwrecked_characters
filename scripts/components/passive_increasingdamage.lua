local Passive_IncreasingDamage = Class(function(self, inst)
    self.inst = inst
	
	self.dmg_lvl = 0
	self.hits = 0
	self.alt_counted = false --Used so that alt AOEs only get counted as 1 attack
	
	inst:ListenForEvent("onattackother", function(inst, data)
		self:NewHit(data.weapon)
	end)
	inst:ListenForEvent("attacked", function()
		self:ResetCount()
	end)
end)

function Passive_IncreasingDamage:NewHit(weapon)
	if self.alt_counted and (not weapon or not weapon.isaltattacking) then
		self.alt_counted = false
	end
	if self.dmg_lvl < 5 and not self.alt_counted then
		if self.hits < 4 then --5th hit boosts dmg_lvl
			self.hits = self.hits + 1
		else
			self.dmg_lvl = self.dmg_lvl + 1
			self.hits = 0
		end
	end
	if weapon and weapon.isaltattacking and not self.alt_counted then
		self.alt_counted = true
	end
end

function Passive_IncreasingDamage:ResetCount()
	self.dmg_lvl = 0
	self.hits = 0
end

return Passive_IncreasingDamage