local RECIPETABS = GLOBAL.RECIPETABS
local Recipe = GLOBAL.Recipe
local TECH = GLOBAL.TECH
local Ingredient = GLOBAL.Ingredient
local State = GLOBAL.State
local Vector3 = GLOBAL.Vector3
local SpawnPrefab = GLOBAL.SpawnPrefab
local COLLISION = GLOBAL.COLLISION

EQUIPSLOTS={HANDS = "hands",HEAD = "head",BODY = "body",BACK = "back",NECK = "neck",}

local prefabs = {
	"surfboard",
	"chefpack",
	"portablecookpot",
	"monstertartare",
	"freshfruitcrepes",
	"luckyhat",
	"luckycutlass",
	"woodlegs_buriedtreasure",

	"walani",
	"warly",
	"wilbur",
	"woodlegs",
	"walani_none", 
	"warly_none", 
	"wilbur_none", 
	"woodlegs_none",
}
local forge_prefabs = {
	"lavaarena_luckyhat",
}
if GLOBAL.TheNet:GetServerGameMode() == "lavaarena" then
	for _,pref in pairs(forge_prefabs) do
		table.insert(prefabs, pref)
	end
end
PrefabFiles = prefabs

Assets = 
{
	--Walani:
	Asset( "IMAGE", "images/avatars/avatar_walani.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_walani.xml" ),
    Asset( "IMAGE", "images/avatars/avatar_ghost_walani.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_walani.xml" ),
	Asset( "IMAGE", "images/avatars/self_inspect_walani.tex" ),
    Asset( "ATLAS", "images/avatars/self_inspect_walani.xml" ),
    Asset( "IMAGE", "bigportraits/walani.tex" ),
    Asset( "ATLAS", "bigportraits/walani.xml" ),
	Asset( "IMAGE", "bigportraits/walani_none.tex" ),
    Asset( "ATLAS", "bigportraits/walani_none.xml" ),
    Asset( "IMAGE", "images/map_icons/walani.tex" ),
    Asset( "ATLAS", "images/map_icons/walani.xml" ),
	Asset("SOUNDPACKAGE", "sound/walani.fev"),
    Asset("SOUND", "sound/walani.fsb"),
	Asset("ANIM", "anim/surfboard.zip"),		
	Asset("IMAGE", "images/map_icons/surfboard.tex"),
	Asset("ATLAS", "images/map_icons/surfboard.xml"),
	--Warly:
	Asset( "IMAGE", "images/avatars/avatar_warly.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_warly.xml" ),
    Asset( "IMAGE", "images/avatars/avatar_ghost_warly.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_warly.xml" ),
	Asset( "IMAGE", "images/avatars/self_inspect_warly.tex" ),
    Asset( "ATLAS", "images/avatars/self_inspect_warly.xml" ),
    Asset( "IMAGE", "bigportraits/warly.tex" ),
    Asset( "ATLAS", "bigportraits/warly.xml" ),
	Asset( "IMAGE", "bigportraits/warly_none.tex" ),
    Asset( "ATLAS", "bigportraits/warly_none.xml" ),
    Asset( "IMAGE", "images/map_icons/warly.tex" ),
    Asset( "ATLAS", "images/map_icons/warly.xml" ),
    Asset("SOUNDPACKAGE", "sound/warly.fev"),
    Asset("SOUND", "sound/warly.fsb"),
    Asset("ANIM", "anim/swap_chefpack.zip"),
    Asset("ANIM", "anim/cook_pot_warly.zip"),
	Asset("IMAGE", "images/map_icons/cookpotwarly.tex"),
	Asset("ATLAS", "images/map_icons/cookpotwarly.xml"),
	Asset("IMAGE", "images/map_icons/spicepack.tex"),
	Asset("ATLAS", "images/map_icons/spicepack.xml"),
	--Wilbur:
	Asset( "IMAGE", "images/avatars/avatar_wilbur.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_wilbur.xml" ),
    Asset( "IMAGE", "images/avatars/avatar_ghost_wilbur.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_wilbur.xml" ),
	Asset( "IMAGE", "images/avatars/self_inspect_wilbur.tex" ),
    Asset( "ATLAS", "images/avatars/self_inspect_wilbur.xml" ),
    Asset( "IMAGE", "bigportraits/wilbur.tex" ),
    Asset( "ATLAS", "bigportraits/wilbur.xml" ),
	Asset( "IMAGE", "bigportraits/wilbur_none.tex" ),
    Asset( "ATLAS", "bigportraits/wilbur_none.xml" ),
    Asset( "IMAGE", "images/map_icons/wilbur.tex" ),
    Asset( "ATLAS", "images/map_icons/wilbur.xml" ),
	Asset("SOUNDPACKAGE", "sound/wilbur.fev"),
	Asset("SOUND", "sound/wilbur.fsb"),
	Asset("ANIM", "anim/wilbur.zip"),
	Asset("ANIM", "anim/wilbur_run.zip"),
	Asset("ANIM", "anim/monkey_projectile.zip"),
    Asset("ANIM", "anim/swap_poop.zip"),
	--Woodlegs:
	Asset( "IMAGE", "images/avatars/avatar_woodlegs.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_woodlegs.xml" ),
    Asset( "IMAGE", "images/avatars/avatar_ghost_woodlegs.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_woodlegs.xml" ),
	Asset( "IMAGE", "images/avatars/self_inspect_woodlegs.tex" ),
    Asset( "ATLAS", "images/avatars/self_inspect_woodlegs.xml" ),
    Asset( "IMAGE", "bigportraits/woodlegs.tex" ),
    Asset( "ATLAS", "bigportraits/woodlegs.xml" ),
	Asset( "IMAGE", "bigportraits/woodlegs_none.tex" ),
    Asset( "ATLAS", "bigportraits/woodlegs_none.xml" ),
    Asset( "IMAGE", "images/map_icons/woodlegs.tex" ),
    Asset( "ATLAS", "images/map_icons/woodlegs.xml" ),
	Asset( "IMAGE", "images/map_icons/woodlegsxspot.tex" ),
    Asset( "ATLAS", "images/map_icons/woodlegsxspot.xml" ),
	Asset("SOUNDPACKAGE", "sound/woodlegs.fev"),
	Asset("SOUND", "sound/woodlegs.fsb"),
	Asset("ANIM", "anim/woodlegs.zip"),
    --
	Asset( "IMAGE", "images/names_gold_walani.tex" ),
    Asset( "ATLAS", "images/names_gold_walani.xml" ),
    Asset( "IMAGE", "images/names_gold_warly.tex" ),
    Asset( "ATLAS", "images/names_gold_warly.xml" ),
	Asset( "IMAGE", "images/names_gold_wilbur.tex" ),
    Asset( "ATLAS", "images/names_gold_wilbur.xml" ),
	Asset( "IMAGE", "images/names_gold_woodlegs.tex" ),
    Asset( "ATLAS", "images/names_gold_woodlegs.xml" ),
	--
	Asset( "IMAGE", "images/names_walani.tex" ),
    Asset( "ATLAS", "images/names_walani.xml" ),
    Asset( "IMAGE", "images/names_warly.tex" ),
    Asset( "ATLAS", "images/names_warly.xml" ),
	Asset( "IMAGE", "images/names_wilbur.tex" ),
    Asset( "ATLAS", "images/names_wilbur.xml" ),
	Asset( "IMAGE", "images/names_woodlegs.tex" ),
    Asset( "ATLAS", "images/names_woodlegs.xml" ),
	
}

if GetModConfigData("wcraftpot") == "y" then
local portablecookpot_item = AddRecipe("portablecookpot_item", {Ingredient("redgem", 3),Ingredient("charcoal", 6),Ingredient("twigs", 4)}, GLOBAL.RECIPETABS.FARM, GLOBAL.TECH.SCIENCE_TWO, nil, nil, nil, nil, "warly", "images/inventoryimages/portablecookpotinv.xml", "portablecookpotinv.tex")
end
local chefpackrecipe = AddRecipe("chefpack", {Ingredient("silk", 3),Ingredient("rope", 2)}, GLOBAL.RECIPETABS.SURVIVAL, GLOBAL.TECH.SCIENCE_ONE, nil, nil, nil, nil, "warly", "images/inventoryimages/chefpack.xml")

local surfboardrecipe = AddRecipe("surfboard", {Ingredient("boards", 4),Ingredient("petals", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.NONE, nil, nil, nil, nil, "walani", "images/inventoryimages/surfboard.xml")
surfboardrecipe.sortkey = 0

local luckyhatrecipe = AddRecipe("luckyhat", {Ingredient("pigskin", 4),Ingredient("boneshard", 4),Ingredient("goldnugget", 10)}, GLOBAL.RECIPETABS.DRESS, GLOBAL.TECH.NONE, nil, nil, nil, nil, "woodlegs", "images/inventoryimages/luckyhat.xml")
luckyhatrecipe.sortkey = 0
local luckycutlassrecipe = AddRecipe("luckycutlass", {Ingredient("flint", 3),Ingredient("twigs", 2),Ingredient("goldnugget", 5)}, GLOBAL.RECIPETABS.WAR, GLOBAL.TECH.NONE, nil, nil, nil, nil, "woodlegs", "images/inventoryimages/luckycutlass.xml")
luckycutlassrecipe.sortkey = -1

--TUNING.WILBURPOOP_DAMAGE = GetModConfigData("wilburpoopdmg")
--move these to strings later
TUNING.LAVAARENA_STARTING_HEALTH.WALANI = 125
TUNING.LAVAARENA_STARTING_HEALTH.WARLY = 125
TUNING.LAVAARENA_STARTING_HEALTH.WILBUR = 150
TUNING.LAVAARENA_STARTING_HEALTH.WOODLEGS = 200
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.WALANI = 1
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.WARLY = 3
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.WILBUR = 2
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.WOODLEGS = 1
TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.WALANI = { "forgedarts", "featheredtunic" }
TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.WARLY = { "forgedarts", "reedtunic" } --riledcrockpot
TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.WILBUR = { "forgedarts", "featheredtunic" } --riledpoop
TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.WOODLEGS = { "pithpike", "forge_woodarmor", "lavaarena_luckyhat" }

if GLOBAL.TheNet:GetServerGameMode() == "lavaarena" then
	TUNING.FORGE.LUCKYHAT = {BONUSDAMAGE = 0.25}
	GLOBAL.STRINGS.LAVAARENA_LUCKYHAT = "Lucky Hat" --Don't know if the name should be changed for Forge?
	GLOBAL.STRINGS.NAMES.LAVAARENA_LUCKYHAT = "Lucky Hat"
	GLOBAL.STRINGS.NAME_DETAIL_EXTENTION.LAVAARENA_LUCKYHAT = "+5% Overall Damage (every 5 hits without taking damage)\nMaximum +25% Overall Damage"
	GLOBAL.STRINGS.FORGED_FORGE.ARMOR.LAVAARENA_LUCKYHAT = {
		NAME = "Lucky Hat",
		DESC = ""
	}
end

modimport("scripts/strings.lua")
--
GLOBAL.ACTIONS.GIVE.oldfn = GLOBAL.ACTIONS.GIVE.fn
GLOBAL.ACTIONS.GIVE.fn = function(act)
    if act.target and act.target.components.surfaction ~= nil then
	if act.doer ~= nil and act.target ~= nil and act.doer:HasTag("walani") and act.target.components.surfaction and not act.doer.components.rider:IsRiding() and not act.doer.components.inventory:IsHeavyLifting() then
		if act.doer.components.moisture then
            act.doer.components.moisture:DoDelta(20)
	    end

		if act.invobject.components.finiteuses ~= nil then
            act.invobject.components.finiteuses:Use(20)
        end
        return act.target.components.surfaction:TeleportAction( act.doer, act.invobject )
	end
	else
    return GLOBAL.ACTIONS.GIVE.oldfn(act)
	end
end

AddComponentAction("USEITEM", "surfaction", function(inst, doer, target, actions)
    if target:HasTag("surfablepond") and not target:HasTag("player") then
	    if doer:HasTag("walani") then
        table.insert(actions, GLOBAL.ACTIONS.GIVE)
		end
    end
end)

AddPrefabPostInit("pond", 	function(inst)	 
if GLOBAL.TheWorld.ismastersim then
local function ItemTradeTest(inst, item)
    if item:HasTag("surfboard") then
        return true
    end
    return false
end
local function ItemGet(inst, giver, item)
	if giver:HasTag("walani") then
    inst.components.surfaction:TeleportAction(giver)
	end
end
    inst:AddTag("surfablepond") 
	inst:AddComponent("surfaction")
	inst:AddComponent("inventory")
    inst:AddComponent("trader")
    inst.components.trader:SetAcceptTest(ItemTradeTest)
    inst.components.trader.onaccept = ItemGet
    inst.components.trader.deleteitemonaccept = false
end
end)

AddMinimapAtlas("images/map_icons/surfboard.xml")
AddMinimapAtlas("images/map_icons/walani.xml")
--
local oldactionstore = GLOBAL.ACTIONS.STORE.strfn
GLOBAL.ACTIONS.STORE.strfn = function(act)
      if act.target ~= nil then if act.target.prefab == "portablecookpot" then
	     return "COOK" 
	  end 
	  end 
	  return oldactionstore(act) 
	  end 
local oldactionpickup = GLOBAL.ACTIONS.PICKUP.fn
GLOBAL.ACTIONS.PICKUP.fn = function(act) 
      if act.doer.components.inventory ~= nil and act.target ~= nil and act.target.components.getitem ~= nil and not act.target:IsInLimbo() then
	  act.doer:PushEvent("onpickupitem", {item = act.target}) return act.target.components.getitem:OnPickup(act.doer) end 
	  return oldactionpickup(act) 
	  end
local function warly_cooker(inst, doer, actions, right) 
     local function CanBePickUp(inst) return not inst:HasTag("donecooking") and not inst:HasTag("readytocook") and inst.replica.container ~= nil 
          and #inst.replica.container:GetItems() == 0 and not inst:HasTag("isopen") and not inst:HasTag("iscooking") 
     end
     if inst.prefab == "portablecookpot" and right and CanBePickUp(inst) then table.insert(actions, GLOBAL.ACTIONS.PICKUP) end 
end
AddComponentAction("SCENE", "stewer", warly_cooker)

local freshfruitcrepes = {		
		name = "freshfruitcrepes",
		test = function(cooker, names, tags) return tags.fruit and tags.fruit >= 1.5 and names.butter and names.honey end,
		priority = 30,
		weight = 1,
		foodtype = "VEGGIE",
		health = TUNING.HEALING_HUGE,
		hunger = TUNING.CALORIES_SUPERHUGE,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_MED,
		cooktime = 2,
	}

local monstertartare = {		
		name = "monstertartare",
		test = function(cooker, names, tags) return tags.monster and tags.monster >= 2 and tags.egg and tags.veggie end,
		priority = 30,
	    weight = 1,
		foodtype = "MEAT",
		health = TUNING.HEALING_SMALL,
		hunger = TUNING.CALORIES_LARGE,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_TINY,
		cooktime = 2,
	}

AddCookerRecipe("portablecookpot",freshfruitcrepes)
AddCookerRecipe("portablecookpot",monstertartare)

AddPrefabPostInit("chefpack", function(inst) --Extra Equipslots auto-compatibility
if not GLOBAL.TheWorld.ismastersim then 
inst.OnEntityReplicated = function(inst) inst.replica.container:WidgetSetup("backpack") end	
return inst
end

local XSLOTS_ADDED = GLOBAL.KnownModIndex:IsModEnabled("workshop-375850593")
for k,v in pairs(GLOBAL.KnownModIndex:GetModsToLoad()) do
	XSLOTS_ADDED = XSLOTS_ADDED or v == "workshop-375850593"
end

if XSLOTS_ADDED then
inst.components.equippable.equipslot = EQUIPSLOTS.BACK
else
inst.components.equippable.equipslot = EQUIPSLOTS.BODY
end

end)
AddMinimapAtlas("images/map_icons/cookpotwarly.xml")
AddMinimapAtlas("images/map_icons/spicepack.xml")
AddMinimapAtlas("images/map_icons/warly.xml")
--
AddMinimapAtlas("images/map_icons/wilbur.xml")
modimport("wilbur_run_temp.lua")
--"Oooh" string stuff
local Oooh_endings = { "", "e", "h" }
local Oooh_punc = { ".", "?", "!" }

local function ooohstart(isstart)
    local str = isstart and "O" or "O"
    local l = math.random(2, 4)
    for i = 2, l do
        str = str..(math.random() > 0.3 and "o" or "a")
    end
    return str
end

local function ooohspace()
    local c = math.random()
    local str =
	    (c <= 1 and " ") or
        (c <= .1 and "! ") or
        (c <= .2 and ". ") or
        (c <= .3 and "? ") or
        (c <= .4 and ", ") or
		(c <= .4 and " ") or
		(c <= 0 and "! ") or
        (c <= .1 and ". ") or
        (c <= .2 and "? ") or
        (c <= .3 and ", ")
    return str, c <= .3
end

local function ooohend()
    return Oooh_endings[math.random(#Oooh_endings)]
end

local function ooohpunc()
    return Oooh_punc[math.random(#Oooh_punc)]
end

local function CraftOooh() -- Monkey speech!
    local isstart = true
    local length = math.random(6)
    local str = ""
    for i = 1, length do
        str = str..ooohstart(isstart)..ooohend()
        if i ~= length then
            local space
            space, isstart = ooohspace()
            str = str..space
        end
    end
    return str..ooohpunc()
end

local _GetSpecialCharacterString = GLOBAL.GetSpecialCharacterString
GLOBAL.GetSpecialCharacterString = function(character)
	character = character and string.lower(character)
	return (character == "wilbur" and CraftOooh()) or _GetSpecialCharacterString(character)
end
--Throw Poop
AddPrefabPostInit("poop", function(inst)

if GLOBAL.TheWorld.ismastersim then

    local onequip = function(inst, owner)
	if owner:HasTag("wilbur") then
        owner.AnimState:OverrideSymbol("swap_object", "swap_poop", "swap_poop")
        owner.AnimState:Show("ARM_carry")
        owner.AnimState:Hide("ARM_normal")
		else --Failsafe
		owner.AnimState:Hide("ARM_carry")
        owner.AnimState:Show("ARM_normal")
	 inst:DoTaskInTime(0, function()
            if owner and owner.components and owner.components.inventory then
               owner.components.inventory:GiveItem(inst)
			end
		end)
		end
    end

    local onunequip = function(inst, owner)
        owner.AnimState:Hide("ARM_carry")
        owner.AnimState:Show("ARM_normal")
    end

    local onthrown = function(inst)
        inst:AddTag("thrown")
		inst.persists = false
        inst.AnimState:SetBank("monkey_projectile")
        inst.AnimState:SetBuild("monkey_projectile")
        inst.AnimState:PlayAnimation("idle", true)

    inst.Physics:SetMass(1)
    inst.Physics:SetCapsule(0.2, 0.2)
    inst.Physics:SetFriction(.2)
    inst.Physics:SetDamping(0)
    inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.ITEMS)
	inst.Physics:SetDontRemoveOnSleep(true)
    end
	
	local onhit = function(inst, thrower)
          local pos = inst:GetPosition()
            if pos.y <= 0.5 then
            local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, 1.5, nil, {"FX", "NOCLICK", "DECOR", "INLIMBO"})
                for k,v in pairs(ents) do
				if (v and v:HasTag("bird")) then
						v.sg:GoToState("stunned")
				end
                    if v.components.combat and not v:HasTag("bird") then
					if not v:HasTag("player") then
                        v.components.combat:GetAttacked(thrower, 10)
					elseif GLOBAL.TheNet:GetPVPEnabled() then
					v.components.combat:GetAttacked(thrower, 10)
                    end
					end
                end
				inst.SoundEmitter:PlaySound("dontstarve/creatures/monkey/poopsplat")
				SpawnPrefab("small_puff").Transform:SetPosition(inst.Transform:GetWorldPosition())
                inst:Remove()
            end
	end
	
	local ReticuleTargetFn = function()
    local player = GLOBAL.ThePlayer
    local ground = GLOBAL.TheWorld.Map
    local pos = Vector3()
    --Attack range is 8, leave room for error
    --Min range was chosen to not hit yourself (2 is the hit range)
    for r = 6.5, 3.5, -.25 do
        pos.x, pos.y, pos.z = player.entity:LocalToWorldSpace(r, 0, 0)
        if ground:IsPassableAtPoint(pos:Get()) and not ground:IsGroundTargetBlocked(pos) then
            return pos
        end
    end
    return pos
    end
		

    local function _Add(inst)
		if inst.components.equippable == nil then
			inst:AddComponent("equippable")
			inst.components.equippable:SetOnEquip(onequip)
            inst.components.equippable:SetOnUnequip(onunequip)
            inst.components.equippable.equipstack = true
		elseif inst.components.complexprojectile == nil then
		   inst:AddComponent("complexprojectile")
           inst.components.complexprojectile:SetHorizontalSpeed(15)
           inst.components.complexprojectile:SetGravity(-35)
           inst.components.complexprojectile:SetLaunchOffset(Vector3(.25, 1, 0))
           inst.components.complexprojectile:SetOnLaunch(onthrown)
	       inst.components.complexprojectile:SetOnHit(onhit)
		elseif inst.components.reticule == nil then
		   inst:AddComponent("reticule")
           inst.components.reticule.targetfn = ReticuleTargetFn
           inst.components.reticule.ease = true
		end
	end
	local function _Remove(inst)
		if inst.components.equippable ~= nil then
			inst:RemoveComponent("equippable")
		elseif inst.components.complexprojectile ~= nil then
		inst:RemoveComponent("complexprojectile")
		elseif inst.components.reticule ~= nil then
		inst:RemoveComponent("reticule")
		end
	end
	local function toinventory(inst, owner)
		if owner ~= nil then
			if owner.prefab == "wilbur" then
				_Add(inst)
			else
				inst:DoTaskInTime(0, _Remove)
			end
		end
	end
	local function toground(inst)
		inst:DoTaskInTime(0, _Remove)
	end
	
	inst:AddComponent("equippable")
			inst.components.equippable:SetOnEquip(onequip)
            inst.components.equippable:SetOnUnequip(onunequip)
            inst.components.equippable.equipstack = true
	
		inst:AddComponent("complexprojectile")
           inst.components.complexprojectile:SetHorizontalSpeed(15)
           inst.components.complexprojectile:SetGravity(-35)
           inst.components.complexprojectile:SetLaunchOffset(Vector3(.25, 1, 0))
           inst.components.complexprojectile:SetOnLaunch(onthrown)
	       inst.components.complexprojectile:SetOnHit(onhit)
		   
		   inst:AddComponent("reticule")
           inst.components.reticule.targetfn = ReticuleTargetFn
           inst.components.reticule.ease = true
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetRange(8, 10)

			inst.components.inventoryitem:SetOnDroppedFn(toground)
    inst.components.inventoryitem:SetOnPickupFn(toinventory)
    inst.components.inventoryitem:SetOnPutInInventoryFn(toinventory)

end

end)
--
function TreasureSanityPostInit(inst)
if GLOBAL.TheWorld.ismastersim then
   local function WoodlegsSanityAura(inst, observer)
   if observer.prefab == "woodlegs" then
      return 100/(30*43)--TUNING.SANITYAURA_TINY
   end
      return 0
   end
    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aurafn = WoodlegsSanityAura
end
end
AddPrefabPostInit("goldnugget", TreasureSanityPostInit)
--AddPrefabPostInit("gem", TreasureSanityPostInit)

AddMinimapAtlas("images/map_icons/woodlegs.xml")
AddMinimapAtlas("images/map_icons/woodlegsxspot.xml")
--
AddModCharacter("walani", "FEMALE")
AddModCharacter("warly", "MALE")
AddModCharacter("wilbur", "NEUTRAL")
AddModCharacter("woodlegs", "MALE")